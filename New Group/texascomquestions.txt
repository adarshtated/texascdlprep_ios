<start>
    <string-array name="q347">
        <item>The load on a vehicle may not extend beyond the front of the vehicle, without a special permit, more than:</item>
        <item>Five feet</item>
        <item>Three feet</item>
        <item>Six feet</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q348">
        <item>The color of clearance lamps on the front of a vehicle must be:</item>
        <item>Green</item>
        <item>Red</item>
        <item>Amber</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q349">
        <item>Trailers and semitrailers must ordinarily have brakes that can be applied by the driver when the gross weight of the trailer exceeds:</item>
        <item>1,000 pounds</item>
        <item>2,000 pounds</item>
        <item>4,500 pounds</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q350">
        <item>The load on a truck may not extend over the rear of the vehicle more than:</item>
        <item>Six feet</item>
        <item>Five feet</item>
        <item>Four feet</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q351">
        <item>All trailers or semitrailers must be equipped with clearance lamps, side marker lamps and side reflectors if their width is:</item>
        <item>80 inches or more</item>
        <item>96 inches or more</item>
        <item>72 inches or more</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q352">
        <item>To find out the details about registering your particular vehicle you should consult:</item>
        <item>County Tax Assessor-Collector</item>
        <item>Railroad Commission</item>
        <item>City Police</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q353">
        <item>When one truck is following another truck or vehicle, it must keep far enough back to allow how many vehicles to overtake and enter safely between them?</item>
        <item>Three vehicles</item>
        <item>Two vehicles</item>
        <item>One vehicle</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q354">
        <item>Turn signal indicators are required on all motor vehicles manufactured after model year:</item>
        <item>1969</item>
        <item>1959</item>
        <item>1971</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q355">
        <item>No combination of vehicles, other than a truck tractor-trailer combination, may exceed:</item>
        <item>65 feet</item>
        <item>50 feet</item>
        <item>40 feet</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q356">
        <item>Vehicles transporting loose material (sand, dirt, gravel, etc) that is capable of spilling or blowing from the vehicle must:</item>
        <item>Always have the load covered to prevent spilling or blowing</item>
        <item>Have a special permit</item>
        <item>Meet loading and/or covering regulations to prevent spilling or blowing</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q357">
        <item>Every farm tractor manufactured after January 1,1972, complies with state lighting laws by:</item>
        <item>Having two head lamps</item>
        <item>Traveling on the shoulder of the road</item>
        <item>Having one white lantern that shows to both the front and rear</item>
        <item>opt1</item>
    </string-array>
	
	<string-array name="q358">
        <item>The maximum speed limit for a taxicab on numbered U.S. or state highways in the daytime is:</item>
        <item>55 mph</item>
        <item>65 mph</item>
        <item>70 mph</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q359">
        <item>To operate on the highway a motor vehicle or trailer having metal tires and weighing 5,000 pounds or more, you must:</item>
        <item>Drive less than 20 mph</item>
        <item>Get a special permit from the Texas Department of Transportation</item>
        <item>Drive in the daytime only</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q360">
        <item>During times when lighted lamps are not required, the driver should set out:</item>
        <item>Three flares</item>
        <item>Two approved red flags</item>
        <item>Three red reflectors</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q361">
        <item>Before going down a steep grade with a commercial vehicle you should:</item>
        <item>Never be travelling more than 5 mph</item>
        <item>Shift to a lower gear</item>
        <item>Remain in the the same gear</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q362">
        <item>Every trailer must have how many reflectors on the rear?</item>
        <item>Two</item>
        <item>Three</item>
        <item>Four</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q363">
        <item>No passenger vehicle may be coupled with more than:</item>
        <item>Two trailers</item>
        <item>One trailer</item>
        <item>Three trailers</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q364">
        <item>When one vehicle is towing another, the distance between the two must not be greater than:</item>
        <item>18 feet</item>
        <item>15 feet</item>
        <item>20 feet</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q365">
        <item>When it is necessary to place flares around a truck on a two-way road with a clear view, one must be placed beside the truck, the other two must be placed:</item>
        <item>50 feet to the front and rear of the vehicle</item>
        <item>100 feet to the front and rear of the vehicle</item>
        <item>200 feet to the front and rear of the vehicle</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q366">
        <item>All school buses, buses, taxis, and other vehicles hauling passengers for hire or lease must carry a chemical type fire extinguisher of at least:</item>
        <item>One quart capacity</item>
        <item>One qallon capacity</item>
        <item>two quart capacity</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q367">
        <item>Every bus or truck manufactured after year model 1959 must be equipped on the rear with:</item>
        <item>Two stoplights</item>
        <item>One red reflector</item>
        <item>One stoplight</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q368">
        <item>When a load extends more than four feet over the rear of a vehicle, what color flag must be attached to the extreme end of the extension if driving in the daytime?</item>
        <item>White</item>
        <item>Red</item>
        <item>Black</item>
        <item>opt2</item>
    </string-array>
   
    <string-array name="q369">
        <item>Which of the following motor vehicles must stop at all railroad grade crossings outside a business or residential district?</item>
        <item>Cattle truck</item>
        <item>Motor bus</item>
        <item>Grain truck</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q370">
        <item>The maximum number of trailers that may be lawfully towed by one vehicle is:</item>
        <item>One</item>
        <item>Three</item>
        <item>Two</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q371">
        <item>The maximum number of trailers that may be towed by a commercial motor vehicle with an unloaded weight in excess of 2,500 pounds is:</item>
        <item>Two trailers</item>
        <item>Three trailers</item>
        <item>One trailer</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q372">
        <item>To haul a load or move equipment that is wider, heavier or longer than law permits, you must:</item>
        <item>request a Highway Patrol escort</item>
        <item>Obtain a permit from the Texas Department of Transportation</item>
        <item>Obtain a permit from the police</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q373">
        <item>When mud flaps are required on trucks or trailers, they must reach how close to the surface of the highway?</item>
        <item>12 inches</item>
        <item>16 inches</item>
        <item>8 inches</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q374">
        <item>Clearance lights are required by state law for trucks or buses if the width is:</item>
        <item>80 inches or more</item>
        <item>65 inches or more</item>
        <item>70 inches or more</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q375">
        <item>It is unlawful to operate any motor vehicle on a highway with:</item>
        <item>More than two mud-grip tires</item>
        <item>Tire chains on the wheels</item>
        <item>Flanges or lugs on the wheels</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q376">
        <item>When towing another vehicle with a chain, rope or cable, what color flag must be attached to the chain, rope or cable?</item>
        <item>Yellow</item>
        <item>Red</item>
        <item>White</item>
        <item>opt3</item>
    </string-array>
 
    <string-array name="q377">
        <item>One purpose of requiring registration papers on trucks at all times is to show:</item>
        <item>Ownership of the truck</item>
        <item>Counties in which the truck may be operated</item>
        <item>Weight of the truck empty and how much it is registered to haul</item>
        <item>opt3</item>
    </string-array>
   
    <string-array name="q378">
        <item>The top speed limit for heavy trucks on designated rural interstates in the nighttime is:</item>
        <item>65 mph</item>
        <item>70 mph</item>
        <item>55 mph</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q379">
        <item>When going downhill with a commercial vehicle, it is unlawful to:</item>
        <item>Remain in the same gear</item>
        <item>Coast</item>
        <item>Shift to a lower gear</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q380">
        <item>Which of the following vehicles are not required to have mud flaps?</item>
        <item>Vehicles hauling livestock</item>
        <item>Pole trailers</item>
        <item>Vehicles operating interstate</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q381">
        <item>What is the maximum speed limit for a taxicab on a numbered U.S. or state highway during the day?</item>
        <item>60 mph</item>
        <item>55 mph</item>
        <item>70 mph</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q382">
        <item>A slow moving vehicle emblem must be displayed only by:</item>
        <item>Delivery trucks which occasionally drive under 25 mph</item>
        <item>All road machinery driving under 25 mph</item>
        <item>Vehicles designed to be operated at speeds of 25 mph or less</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q383">
        <item>The first thing the driver of a disabled truck or bus should do is:</item>
        <item>Go for help</item>
        <item>Set out flares, reflectors or flags</item>
        <item>Try to get the vehicle started</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q384">
        <item>The length requirements of state law for vehicles or combinations of vehicles, including extensions over the front and rear, do not apply:</item>
        <item>Within city limits</item>
        <item>On farm to market highways</item>
        <item>On state highways</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q385">
        <item>The height from the ground for mounted reflectors must be at least:</item>
        <item>30 inches</item>
        <item>24 inches</item>
        <item>18 inches</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q386">
        <item>When turning right with a long vehicle or combination truck-trailer, if it is impossible to stay in the proper lane, you should:</item>
        <item>Approach the corner 12 feet from the curb or right edge of the roadway, then turn into the right-hand lane</item>
        <item>Approach the corner in the center of the street then turn right into the right-hand lane</item>
        <item>Approach the corner about 4 feet from the curb or right edge of the roadway, then, if necessary, complete the turn in the center of the street entered</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q387">
        <item>Clearance lamps mounted on the rear or on the side near the rear of a vehicle must be what color?</item>
        <item>White</item>
        <item>Red</item>
        <item>Amber</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q388">
        <item>When stopped on the side of a divided highway, flags or reflectors must be placed:</item>
        <item>One beside the vehicle, one 50 feet in front and one 50 feet behind the vehicle</item>
        <item>One beside the vehicle, one 100 feet in front and one 100 feet behind the vehicle</item>
        <item>One 10 feet behind the vehicle, one 100 feet behind and one 200 feet behind the vehicle</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q389">
        <item> The maximum speed limit for a pickup truck with a manufacturer's rated carrying capacity of 2,000 lbs or less on U.S. or state highways in the daytime is:</item>
        <item>70 mph</item>
        <item>55 mph</item>
        <item>65 mph</item>
        <item>opt1</item>
    </string-array>

    <string-array name="q390">
        <item>When backing a large truck you should:</item>
        <item>Sound your horn to clear the way and back very slowly</item>
        <item>Have someone guide you</item>
        <item>Look straight ahead so you can back straight</item>
        <item>opt2</item>
    </string-array>

    <string-array name="q391">
        <item>The greatest weight allowed by state law for any vehicle including its load is:</item>
        <item>60,000 pounds</item>
        <item>36,400 pounds</item>
        <item>80,000 pounds</item>
        <item>opt3</item>
    </string-array>

    <string-array name="q392">
        <item>The greatest height ordinarily allowed by state law for a vehicle including its load is:</item>
        <item>12 feet</item>
        <item>14 feet</item>
        <item>12 feet, six inches</item>
        <item>opt2</item>
    </string-array>
</start>
