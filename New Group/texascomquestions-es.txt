<start>
<string-array>
<item>La carga en un vehículo no puede extenderse más allá de la parte delantera del vehículo, sin un permiso especial, más que:</item>
<item>Five feet</item>
<item>Three feet</item>
<item>Six feet</item>
<item>opt2</item>
</string-array>


<string-array>
<item>El color de las luces de paso en la parte delantera de un vehículo debe ser:</item>
<item>Green</item>
<item>Red</item>
<item>Amber</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Los remolques y semirremolques normalmente deben tener frenos que pueden ser aplicados por el conductor cuando el peso bruto del remolque excede:</item>
<item>1,000 libras</item>
<item>2,000 libras</item>
<item>4,500 libras</item>
<item>opt3</item>
</string-array>


<string-array>
<item>La carga en un camión no puede extenderse sobre la parte trasera del vehículo más de:</item>
<item>Six feet</item>
<item>Five feet</item>
<item>Four feet</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Todos los remolques o semirremolques deben estar equipados con luces de posición, luces de posición laterales y reflectores laterales si su ancho es:</item>
<item>80 pulgadas o más</item>
<item>96 pulgadas o más</item>
<item>72 pulgadas o más</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Para conocer los detalles sobre el registro de su vehículo en particular, debe consultar:</item>
<item>Assesor-Collector del condado</item>
<item>Comisión del ferrocarril</item>
<item>City Police</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Cuando un camión sigue a otro camión o vehículo, debe mantenerse lo suficientemente atrás para permitir cuántos vehículos adelantar y entrar de manera segura entre ellos?</item>
<item>Tres vehículos</item>
<item>Dos vehículos</item>
<item>Un vehículo</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Se requieren indicadores de señal de giro en todos los vehículos motorizados fabricados después del año modelo:</item>
<item>1969</item>
<item>1959</item>
<item>1971</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Ninguna combinación de vehículos, excepto una combinación de camión y tractor, puede exceder:</item>
<item>65 feet</item>
<item>50 pies</item>
<item>40 feet</item>
<item>opt1</item>
</string-array>


<string-array name="q10">
<item>Los vehículos que transportan material suelto (arena, tierra, grava, etc.) que puede derramarse o soplarse desde el vehículo deben:</item>
<item>Cubra siempre la carga para evitar derrames o soplos</item>
<item>Tener un permiso especial</item>
<item>Cumple con las regulaciones de carga y / o cobertura para evitar derrames o soplos</item>
<item>opt3</item>
</string-array>



<string-array>
<item>Cada tractor agrícola fabricado después del 1 de enero de 1972, cumple con las leyes estatales de iluminación por:</item>
<item>Con dos lámparas de cabeza</item>
<item>Viajando en el borde de la carretera</item>
<item>Con una linterna blanca que se muestra en la parte delantera y trasera</item>
<item>opt1</item>
</string-array>


<string-array>
<item>El límite de velocidad máxima para un taxi en Estados Unidos o carreteras estatales numeradas durante el día es:</item>
<item>55 mph</item>
<item>65 mph</item>
<item>70 mph</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Para operar en una carretera un vehículo motorizado o un remolque que tenga llantas de metal y que pese 5,000 libras o más, debe:</item>
<item>Conduzca a menos de 20 mph</item>
<item>Obtenga un permiso especial del Departamento de Transporte de Texas</item>
<item>Conduzca solo durante el día</item>
<item>opt2</item>
</string-array>


<string-array>
<item>En los momentos en que no se requieren lámparas encendidas, el conductor debe indicar:</item>
<item>Tres bengalas</item>
<item>Dos banderas rojas aprobadas</item>
<item>Tres reflectores rojos</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Antes de bajar una pendiente con un vehículo comercial, debe:</item>
<item>Nunca viaje a más de 5 mph</item>
<item>Cambie a una velocidad más baja</item>
<item>Permanecer en la misma marcha</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cada remolque debe tener cuántos reflectores en la parte trasera?</item>
<item>Two</item>
<item>Three</item>
<item>Four</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Ningún vehículo de pasajeros puede estar acoplado con más de:</item>
<item>Dos remolques</item>
<item>Un trailer</item>
<item>Three trailers</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cuando un vehículo está remolcando otro, la distancia entre los dos no debe ser mayor que:</item>
<item>18 pies</item>
<item>15 feet</item>
<item>20 pies</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cuando es necesario colocar bengalas alrededor de un camión en una carretera de dos vías con una vista despejada, una debe colocarse al lado de la camioneta, las otras dos deben colocarse:</item>
<item>50 pies hacia la parte delantera y trasera del vehículo</item>
<item>100 pies hacia la parte delantera y trasera del vehículo</item>
<item>200 pies hacia la parte delantera y trasera del vehículo</item>
<item>opt2</item>
</string-array>


<string-array name = "q20">
<item>Todos los autobuses escolares, autobuses, taxis y otros vehículos que transporten o arriendan a los pasajeros deben llevar un extintor de incendios de tipo químico de al menos:</item>
<item>Capacidad de un cuarto</item>
<item>Una capacidad qallon</item>
<item>capacidad de dos cuartos</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Cada autobús o camión fabricado después del año 1959 debe estar equipado con:</item>
<item>Dos luces de parada</item>
<item>Un reflector rojo</item>
<item>One stop light</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Cuando una carga se extiende más de cuatro pies sobre la parte trasera de un vehículo, ¿qué bandera de color se debe colocar en el extremo de la extensión si se conduce durante el día?</item>
<item>White</item>
<item>Red</item>
<item>Black</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cuál de los siguientes vehículos motorizados debe detenerse en todos los cruces de nivel ferroviario fuera de un distrito comercial o residencial?</item>
<item>Cattle truck</item>
<item>Bus del motor</item>
<item>Grain truck</item>
<item>opt2</item>
</string-array>


<string-array>
<item>El número máximo de remolques que un vehículo puede remolcar legalmente es:</item>
<item>One</item>
<item>Three</item>
<item>Two</item>
<item>opt3</item>
</string-array>


<string-array>
<item>El número máximo de remolques que puede remolcar un vehículo de motor comercial con un peso descargado en exceso de 2,500 libras es:</item>
<item>Dos remolques</item>
<item>Three trailers</item>
<item>Un trailer</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Para transportar una carga o mover un equipo más ancho, más pesado o más largo de lo que permite la ley, debe:</item>
<item>solicita una escolta de Patrulla de Carreteras</item>
<item>Obtenga un permiso del Departamento de Transporte de Texas</item>
<item>Obtenga un permiso de la policía</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cuando se requieren faldones de lodo en camiones o remolques, deben alcanzar la superficie de la carretera?</item>
<item>12 pulgadas</item>
<item>16 pulgadas</item>
<item>8 pulgadas</item>
<item>opt3</item>
</string-array>


<string-array>
<item>La ley estatal requiere luces de espacio para camiones o autobuses si el ancho es:</item>
<item>80 pulgadas o más</item>
<item>65 pulgadas o más</item>
<item>70 pulgadas o más</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Es ilegal operar cualquier vehículo motorizado en una carretera con:</item>
<item>Más de dos neumáticos de agarre de lodo</item>
<item>Cadenas de neumáticos en las ruedas</item>
<item>Bridas o lengüetas en las ruedas</item>
<item>opt3</item>
</string-array>


<string-array name="q30">
<item>Al remolcar otro vehículo con una cadena, cuerda o cable, qué bandera de color debe estar adherida a la cadena, cuerda o cable?</item>
<item>Yellow</item>
<item>Red</item>
<item>White</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Uno de los propósitos de requerir documentos de registro en camiones en todo momento es mostrar:</item>
<item>Propiedad del camión</item>
<item>Condados en los que se puede operar el camión</item>
<item>Peso del camión vacío y cuánto está registrado para transportar</item>
<item>opt3</item>
</string-array>


<string-array>
<item>El límite de velocidad máxima para camiones pesados ​​en las carreteras interestatales rurales designadas durante la noche es:</item>
<item>65 mph</item>
<item>70 mph</item>
<item>55 mph</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Al ir cuesta abajo en un vehículo comercial, es ilegal:</item>
<item>Permanecer en la misma marcha</item>
<item>Coast</item>
<item>Cambie a una velocidad más baja</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cuál de los siguientes vehículos no requiere tener aletas de barro?</item>
<item>Vehículos que transportan ganado</item>
<item>Remolques para postes</item>
<item>Vehículos que operan en un estado a otro</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cuál es el límite de velocidad máxima para un taxi en una autopista estatal o estatal numerada durante el día?</item>
<item>60 mph</item>
<item>55 mph</item>
<item>70 mph</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Un emblema de vehículo de movimiento lento solo se debe mostrar mediante:</item>
<item>Camiones de reparto que ocasionalmente conducen a menos de 25 mph</item>
<item>Todas las máquinas de carretera que conducen a menos de 25 mph</item>
<item>Vehículos diseñados para ser operados a velocidades de 25 mph o menos</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Lo primero que debe hacer el conductor de un camión o autobús deshabilitado es:</item>
<item>Go for help</item>
<item>Establecer destellos, reflectores o banderas</item>
<item>Intente arrancar el vehículo</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Los requisitos de longitud de la ley estatal para vehículos o combinaciones de vehículos, incluidas las extensiones en la parte delantera y trasera, no se aplican:</item>
<item>Dentro de los límites de la ciudad</item>
<item>En las carreteras de la granja al mercado</item>
<item>En carreteras estatales</item>
<item>opt1</item>
</string-array>


<string-array>
<item>La altura desde el suelo para los reflectores montados debe ser al menos:</item>
<item>30 pulgadas</item>
<item>24 pulgadas</item>
<item>18 pulgadas</item>
<item>opt2</item>
</string-array>


<string-array name = "q40">
<item>Al girar a la derecha con un vehículo largo o un camión con remolque combinado, si es imposible permanecer en el carril adecuado, debe:</item>
<item>Acérquese a la esquina a 12 pies de la acera o el borde derecho de la carretera, luego gire hacia el carril de la derecha</item>
<item>Acérquese a la esquina en el centro de la calle, luego gire a la derecha en el carril derecho</item>
<item>Acérquese a la esquina a unos 4 pies de la acera o el borde derecho de la carretera, luego, si es necesario, complete el giro en el centro de la calle ingresado</item>
<item>opt3</item>
</string-array>


<string-array>
<item>Las luces de paso montadas en la parte trasera o en el lado cerca de la parte trasera de un vehículo deben ser de qué color?</item>
<item>White</item>
<item>Red</item>
<item>Amber</item>
<item>opt2</item>
</string-array>


<string-array>
<item>Cuando se detiene en el lado de una carretera dividida, deben colocarse banderas o reflectores:</item>
<item>Uno al lado del vehículo, uno 50 pies delante y otro 50 pies detrás del vehículo</item>
<item>Uno al lado del vehículo, uno a 100 pies delante y otro a 100 pies detrás del vehículo</item>
<item>Un 10 pies detrás del vehículo, uno 100 pies detrás y otro 200 pies detrás del vehículo</item>
<item>opt3</item>
</string-array>


<string-array>
<item>El límite de velocidad máxima para una camioneta con una capacidad de carga nominal de un fabricante de 2,000 libras o menos en los EE. UU. o en las carreteras estatales durante el día es:</item>
<item>70 mph</item>
<item>55 mph</item>
<item>65 mph</item>
<item>opt1</item>
</string-array>


<string-array>
<item>Al retroceder un camión grande, debe:</item>
<item>Haga sonar su bocina para despejar el camino y retroceder muy lentamente</item>
<item>Haga que alguien lo guíe</item>
<item>Mire hacia adelante para que pueda retroceder</item>
<item>opt2</item>
</string-array>


<string-array>
<item>El mayor peso permitido por la ley estatal para cualquier vehículo, incluida su carga, es:</item>
<item>60,000 libras</item>
<item>36,400 libras</item>
<item>80,000 libras</item>
<item>opt3</item>
</string-array>


<string-array name = "q46">
<item>La altura máxima normalmente permitida por la ley estatal para un vehículo, incluida su carga, es:</item>
<item>12 feet</item>
<item>14 feet</item>
<item>12 pies, seis pulgadas</item>
<item>opt2</item>
</string-array>


</start>
