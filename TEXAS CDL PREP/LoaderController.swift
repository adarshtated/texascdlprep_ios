//
//  LoaderController.swift
//  VetChannel
//
//  Created by Apple on 30/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LoaderController: NSObject {
    
    static let sharedInstance = LoaderController()
    private let activityIndicator = UIActivityIndicatorView()
    
    private func setupLoader() {
        removeLoader()
        
        activityIndicator.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            activityIndicator.style = UIActivityIndicatorView.Style.large
        } else {
            // Fallback on earlier versions
        }
        activityIndicator.color = UIColor.black
    }
    
    func showLoader() {
        setupLoader()
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        let holdingView = appDel.window!.rootViewController!.view!
        
        DispatchQueue.main.async {
            self.activityIndicator.center = holdingView.center
            self.activityIndicator.startAnimating()
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                    topController.view.addSubview(self.activityIndicator)
                    topController.view.bringSubviewToFront(self.activityIndicator)
                }

            // topController should now be your topmost view controller
            }
//            holdingView.addSubview(self.activityIndicator)
//            holdingView.bringSubviewToFront(self.activityIndicator)
//            self.activityIndicator.isUserInteractionEnabled = false
            self.activityIndicator.superview?.isUserInteractionEnabled = false
        }
    }
    
    func removeLoader(){
        DispatchQueue.main.async {
            self.activityIndicator.superview?.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
//            self.activityIndicator.isUserInteractionEnabled = true
        }
    }
}

