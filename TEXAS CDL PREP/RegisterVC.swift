//
//  RegisterVC.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 26/04/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var txtUserName: CommonTextfield!
    @IBOutlet weak var txtEmail: CommonTextfield!
    @IBOutlet weak var txtPassword: CommonTextfield!
    @IBOutlet weak var btnRegister: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Registration"
        
        if #available(iOS 13.0, *) {
            txtUserName.setImage(image: UIImage(systemName: "person.crop.rectangle.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 13.0, *) {
            txtEmail.setImage(image: UIImage(systemName: "envelope.fill")!, leftPadding: 20)//person.fill
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 13.0, *) {
            txtPassword.setImage(image: UIImage(systemName: "lock.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        btnRegister.backgroundColor = appcolor
        btnRegister.layer.cornerRadius = 20
        setUpMenuButton()
        setUpRightMenuButton()

    }
    
    @IBAction func onClickRegister(_ sender: Any) {
        callRegApi()
    }
    @IBAction func onClickLogin(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func callRegApi(){
        if txtEmail.text == "" || txtPassword.text == "" || txtUserName.text == ""{
            showAlertView(title: "Alert", message: "Cannot leave any field empty.")
            return;
        }
        if txtUserName.text!.contains(" "){
            showAlertView(title: "Alert", message: "Username should not contain space")
            return;
        }
        fetchGenericData(urlString: "registration", parameters: "username=\(txtUserName.text!)&email=\(txtEmail.text!)&password=\(txtPassword.text!)", completion: {(objCommonResponse: Reg?, str) in
            DispatchQueue.main.async {
                if let objCommonResponse = objCommonResponse{
                    if objCommonResponse.status {
                        //self.showAlertView(title: "Alert", message: objCommonResponse.message)
                        IAPHandler.shared.tryCheckValidateReceiptAndUpdateExpirationDate()
                        UserDefaults.standard.setValue(objCommonResponse.result![0].user_id, forKey: "userId")
                        UserDefaults.standard.synchronize()
                        
                        let next = self.storyboard?.instantiateViewController(withIdentifier: "StartPageViewController") as! StartPageViewController
                        self.navigationController?.pushViewController(next, animated: true)
                        self.getAllOrder(showAlert: true)
                    }else{
                        self.showAlertView(title: "Alert", message: objCommonResponse.message)
                        LoaderController.sharedInstance.removeLoader()
                    }
                }else{
                    LoaderController.sharedInstance.removeLoader()
                }
            }
        })
    }
    

}
