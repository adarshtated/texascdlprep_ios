//
//  LaunchScreenViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 06/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {

    @IBOutlet var logo: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("isPopupShow",UserDefaults.standard.object(forKey: "isPopupShow"))
        
        if UserDefaults.standard.object(forKey: "isPopupShow") as? String == "false"{
            self.backView.isHidden = true
        }
        
        
        topView.layer.cornerRadius = 15
        topView.layer.masksToBounds = true
        
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            print("finished rotating")
            if UserDefaults.standard.object(forKey: "isPopupShow") as? String == "false"{
                let next = self.storyboard?.instantiateViewController(withIdentifier: "NavViewController") as! NavViewController
                next.modalPresentationStyle = .fullScreen
                self.present(next, animated: true, completion: nil)
            }
        })
        let rotationAnimation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: .pi * 2.0)
        rotationAnimation.duration = 2;
        rotationAnimation.repeatCount = 0;
        self.logo?.layer.add(rotationAnimation, forKey: "rotationAnimation")
        CATransaction.commit()
        
    }
    @IBAction func btnOk(_ sender: Any) {
        self.backView.isHidden = true
        UserDefaults.standard.setValue("true", forKey: "isPopupShow")
        let next = self.storyboard?.instantiateViewController(withIdentifier: "NavViewController") as! NavViewController
        next.modalPresentationStyle = .fullScreen
        self.present(next, animated: true, completion: nil)
    }
}
