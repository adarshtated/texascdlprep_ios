//
//  NavViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 02/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class NavViewController: UINavigationController {

    static var savedAnswer = NSMutableArray()
    static var quiz1 = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        //HTML code:    #0B406F
//        RGB code:    R: 11 G: 64 B: 111
//        HSV:    208.2° 90.09% 43.53%
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
}
