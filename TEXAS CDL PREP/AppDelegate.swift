//
//  AppDelegate.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 02/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        UserDefaults.standard.set(1, forKey: "quiz0")
//        UserDefaults.standard.set("2020-12-02", forKey: "quiz11_expDate")
//        UserDefaults.standard.synchronize()
        IQKeyboardManager.shared.enable = true
        if UserDefaults.standard.object(forKey: "isEnglish") == nil{
            UserDefaults.standard.set(true, forKey: "isEnglish")
        }
        
        if UserDefaults.standard.object(forKey: "isPopupShow") as? String == "true"{
            UserDefaults.standard.setValue("false", forKey: "isPopupShow")
        }
        UserDefaults.standard.setValue("", forKey: "InAppRefresh")
//        if UserDefaults.standard.value(forKey: "isPurchased") as? String == "true"{
//
        
//        }
        
        
//        UserDefaults.standard.setValue("2020-12-27", forKey: "quiz5_expDate")//2020-12-27
//        for i in 0..<12{
//            print("quiz\(i)  key exist? \(isKeyPresentInUserDefaults(key: "quiz\(i)"))  expDate ------ "+"\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? "")")
//        }
//        UserDefaults.standard.removeObject(forKey: "quiz11_expDate")
        
        
        
        //
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

