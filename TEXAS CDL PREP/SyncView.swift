//
//  LoginView.swift
//  TEXAS CDL PREP
//
//  Created by Sejal on 08/07/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
var isDataGetEmpty = true

class SyncView: UIView {
    
    let lblSync = UILabel()
    let btnSync = UIButton(type: .system)
    let imgView = UIImageView()
    let btnCross = UIButton(type: .system)
    let blackview = UIView()
    var topview: UIView!
    var vc: UIViewController!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(frame: CGRect, viewController: UIViewController, topView: UIView, hideRadio: Bool? = false) {
        self.init(frame: frame)
        topview = topView
        vc = viewController
        lblSync.text = "Do you want to sync your details?"
        lblSync.numberOfLines = 0
        lblSync.textAlignment = .center
        btnSync.setTitle("Sync", for: .normal)
        imgView.tintColor = .black
        
        btnSync.backgroundColor = appcolor
        btnSync.setTitle("Sync Now", for: .normal)
        btnSync.tintColor = .white
        btnSync.layer.cornerRadius = 20
        btnCross.setImage(UIImage(named: "close.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnCross.tintColor = appcolor
        if #available(iOS 13.0, *) {
            imgView.image = UIImage(systemName: "arrow.2.circlepath")
        } else {
            // Fallback on earlier versions
        }
        blackview.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
        setUpView(viewController: viewController, topView: topView)
    }
    
    func setUpView(viewController: UIViewController, topView: UIView){
        
        let topSpace = CGFloat(15)
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 5
        
        topView.addSubview(blackview)
        blackview.anchor(top: topView.safeAreaLayoutGuide.topAnchor, paddingTop: 0, bottom: topView.safeAreaLayoutGuide.bottomAnchor, paddingBottom: 0, left: topView.leftAnchor, paddingLeft: 0, right: topView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        
        blackview.addSubview(self)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalTo: blackview.widthAnchor, multiplier: 0.8).isActive = true
        self.centerXAnchor.constraint(equalTo: blackview.centerXAnchor, constant: 0).isActive = true
        self.centerYAnchor.constraint(equalTo: blackview.centerYAnchor, constant: 0).isActive = true
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.backgroundColor = .white
        
        self.addSubview(btnCross)
        btnCross.anchor(top: self.topAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: 50, height: 50)
        btnCross.addTarget(self, action: #selector(removeDynamicView), for: .touchUpInside)
        
        self.addSubview(lblSync)
        lblSync.anchor(top: self.topAnchor, paddingTop: topSpace * 2, bottom: nil, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: 0, height: 0)
        
        self.addSubview(imgView)
        imgView.anchor(top: lblSync.bottomAnchor, paddingTop: topSpace, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 25, height: 25)
        imgView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(btnSync)
        btnSync.anchor(top: imgView.bottomAnchor, paddingTop: topSpace, bottom: self.bottomAnchor, paddingBottom: topSpace, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 120, height: 40)
        btnSync.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        btnSync.addTarget(self, action: #selector(callSyncApi), for: .touchUpInside)
        
    }
    
    @objc func removeDynamicView(){
        self.removeFromSuperview()
        self.blackview.removeFromSuperview()
    }
    
    @objc func callSyncApi(){
        vc?.getAllOrder(showAlert: true)
        self.removeDynamicView()
    }
    
    deinit {
        print("sync view deinit")
    }
}


extension UIViewController{
    func getAllOrder(showAlert: Bool? = false){
        var alreadyPurchasedQuiz: [Int] = []
        fetchGenericData(urlString: "all_order", parameters: "customer_id=\(UserDefaults.standard.value(forKey: "userId") ?? "")", completion: {(objCommonResponse: AllOrdersLogin?, str) in
            DispatchQueue.main.async { [self] in
                if let objCommonResponse = objCommonResponse{
                    if objCommonResponse.status {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        if let results = objCommonResponse.result{
                            print("results",results.count)
                            //will be sorted by product names
                            var filteredResults = results.sorted { (a1, b1) -> Bool in
                                //print("filteredResults~~~~~",a1.expire_date ?? "",b1.expire_date ?? "")
                                return a1.product_name < b1.product_name
                            }
                            //print("filteredResults",filteredResults.count)
                            var removeIndex: [Int] = []
                            for i in 0..<filteredResults.count - 1{
                                if i + 1 < filteredResults.count{
                                    for j in i + 1..<filteredResults.count{
//                                        print(i,j)
                                        if filteredResults[i].product_name == filteredResults[j].product_name{
                                            //print("i: \(i) name: \(filteredResults[i].product_name),\(filteredResults[j].product_name) expDate: \(filteredResults[i].expire_date ?? ""),\(filteredResults[j].expire_date ?? "")")
                                            if (filteredResults[i].expire_date ?? "").isEmpty && (filteredResults[j].expire_date ?? "").isEmpty{ // both are empty - remove extra
                                                print("~~~~~~~~~~1")
                                                if !removeIndex.contains(j){
                                                    removeIndex.append(j)
                                                }
                                            }else if !(filteredResults[i].expire_date ?? "").isEmpty && (filteredResults[j].expire_date ?? "").isEmpty{ //1st not empty 2nd empty - this in case of wrong inserted data or user purchasing even though he already has lifetime access - allow lifetime access
                                               // print("~~~~~\(i)~\(j)~~~~2")
                                                if !removeIndex.contains(j){
                                                    removeIndex.append(j)
                                                }
                                            }else if (filteredResults[i].expire_date ?? "").isEmpty && !(filteredResults[j].expire_date ?? "").isEmpty{ // 1st empty and 2nd not empty - this in case of wrong inserted data or user purchasing even though he already has lifetime access - allow lifetime access
                                                print("~~~~~~~~~~3")
                                                if !removeIndex.contains(j){
                                                    removeIndex.append(j)
                                                }
                                            }else if !(filteredResults[i].expire_date ?? "").isEmpty && !(filteredResults[j].expire_date ?? "").isEmpty{ // both are not empty - monthly subscription running, check greatest expiry date
                                                //print("both are not empty----\(i)-\(j)-\(filteredResults[i].product_name)~\(filteredResults[i].expire_date)~~~~\(filteredResults[j].product_name)~\(filteredResults[j].expire_date)")
                                                if let exp1 = formatter.date(from: filteredResults[i].expire_date ?? ""), let exp2 = formatter.date(from: filteredResults[j].expire_date ?? ""){
                                                    if exp1 > exp2{
                                                        if !removeIndex.contains(j){
                                                            removeIndex.append(j)
                                                        }
                                                    }else{
                                                        if !removeIndex.contains(i){
                                                            removeIndex.append(i)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
//                                print(" i \(i)---- \(filteredResults[i].product_name)")
                            }
                            let sortedIndex0 = removeIndex.sorted()//.reversed()
                            let sortedIndex = sortedIndex0.reversed()
                            print("sortedIndex: \(sortedIndex)")
                            print("filteredResults.count: \(filteredResults.count)")
//                            for obj in filteredResults{
//                                print("name~~~~~: \(obj.product_name) expDate: \(obj.expire_date ?? "")")
//                            }
                            for val in sortedIndex{
                                //print("val",val)
                                filteredResults.remove(at: val)
                            }
                            print("filteredResults.count after removing: \(filteredResults.count)")
                            
                            
                            
                            
                            
                            for obj in filteredResults{
                                print("name: \(obj.product_name) expDate: \(obj.expire_date ?? "")")
                                
                                if obj.product_name == "Commercial Vehicle"{
                                    let txtDate = UserDefaults.standard.value(forKey: "CVExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "commercial-vehicle", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "commercial-vehicle", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "General Knowledge"{
                                    let txtDate = UserDefaults.standard.value(forKey: "GKExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "general-knowledge", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "general-knowledge", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Full Key Package"{
                                    let txtDate = UserDefaults.standard.value(forKey: "FKPExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "full-key", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "full-key", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Permit Pass Package"{
                                    let txtDate = UserDefaults.standard.value(forKey: "PPPExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "permit-pass", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "permit-pass", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Pre-Trip Inspection"{
                                    let txtDate = UserDefaults.standard.value(forKey: "PIExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "pre-trip", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "pre-trip", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Air Brake Endorsement"{
                                    let txtDate = UserDefaults.standard.value(forKey: "AEExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "air-brake", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "air-brake", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Combination"{
                                    let txtDate = UserDefaults.standard.value(forKey: "CKPExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "combination", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "combination", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Hazmat Endorsement"{
                                    let txtDate = UserDefaults.standard.value(forKey: "HEExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "hazmat", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "hazmat", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Tanker Endorsement"{
                                    let txtDate = UserDefaults.standard.value(forKey: "TEExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "tanker", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "tanker", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Double / Triples Endorsement"{
                                    let txtDate = UserDefaults.standard.value(forKey: "DTExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "doubles-triples", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "doubles-triples", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "School Bus Endorsement"{
                                    let txtDate = UserDefaults.standard.value(forKey: "SBExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "school-bus", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "school-bus", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }else if obj.product_name == "Passenger Endorsement"{
                                    let txtDate = UserDefaults.standard.value(forKey: "PBExpires_date") as? String ?? ""
                                    if let syncExpDate = formatter.date(from: txtDate), let savedExpDate = formatter.date(from: obj.expire_date ?? ""){
                                        if syncExpDate > savedExpDate{
                                            selectQuizViewController.updateExpireDate(productTag: "passenger", productExpiry: "\(txtDate)")
                                        }
                                    }else if obj.expire_date ?? "" == "" && txtDate != ""{
                                        let exp_date = formatter.date(from: txtDate)
                                        if Date() < exp_date ?? Date(){
                                            selectQuizViewController.updateExpireDate(productTag: "passenger", productExpiry: "\(txtDate)")
                                        }
                                    }
                                }
                            }
                            
                            for obj in filteredResults{
                                if obj.product_name == "Commercial Vehicle"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(5)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 5){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz5_expDate")
                                        alreadyPurchasedQuiz.append(5)
                                    }
                                }else if obj.product_name == "General Knowledge"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(3)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 3){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz3_expDate")
                                        alreadyPurchasedQuiz.append(3)
                                    }
                                }else if obj.product_name == "Full Key Package"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(0)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(2)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(3)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(4)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(5)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(6)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(7)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(8)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(9)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(10)")
//                                    UserDefaults.standard.set(1, forKey: "quiz\(11)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 0){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz0_expDate")
                                        
                                        alreadyPurchasedQuiz.append(0)
                                    }
                                }else if obj.product_name == "Permit Pass Package"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(1)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 1){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz1_expDate")
                                        alreadyPurchasedQuiz.append(1)
                                    }
                                }else if obj.product_name == "Pre-Trip Inspection"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(4)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 4){
                                        print("Pre-Trip Inspection~~~~",UserDefaults.standard.value(forKey: "quiz\(4)"))
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz4_expDate")
                                        alreadyPurchasedQuiz.append(4)
                                    }
                                }else if obj.product_name == "Air Brake Endorsement"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(6)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 6){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz6_expDate")
                                        alreadyPurchasedQuiz.append(6)
                                    }
                                }else if obj.product_name == "Combination"{
                                    print("~~~~1~~Combination~~~1~~~~~",obj.expire_date)

                                    UserDefaults.standard.set(1, forKey: "quiz\(2)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 2){
                                        print("~~~~~~Combination~~~~~~~~",obj.expire_date)
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz2_expDate")
                                        print("Combination~~~~~~~~",UserDefaults.standard.value(forKey: "quiz2_expDate") ?? "")
                                        alreadyPurchasedQuiz.append(2)
                                    }
                                }else if obj.product_name == "Hazmat Endorsement"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(7)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 7){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz7_expDate")
                                        alreadyPurchasedQuiz.append(7)
                                    }
                                }else if obj.product_name == "Tanker Endorsement"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(8)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 8){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz8_expDate")
                                        alreadyPurchasedQuiz.append(8)
                                    }
                                }else if obj.product_name == "Double / Triples Endorsement"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(10)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 10){
                                        print("Double / Triples Endorsement",obj.expire_date,UserDefaults.standard.value(forKey: "quiz\(10)"))
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz10_expDate")
                                        alreadyPurchasedQuiz.append(10)
                                    }
                                }else if obj.product_name == "School Bus Endorsement"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(11)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 11){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz11_expDate")
                                        alreadyPurchasedQuiz.append(11)
                                    }
                                }else if obj.product_name == "Passenger Endorsement"{
                                    UserDefaults.standard.set(1, forKey: "quiz\(9)")
                                    if saveNewDate(newDate: obj.expire_date ?? "", quizNumber: 9){
                                        UserDefaults.standard.setValue(obj.expire_date, forKey: "quiz9_expDate")
                                        alreadyPurchasedQuiz.append(9)
                                    }
                                }
                                if showAlert ?? false{
                                    let ac = UIAlertController(title: "Alert", message: "Products fetched successfully.", preferredStyle: .alert)
                                    ac.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                    //present(ac, animated: true, completion: nil)
                                }
                                UserDefaults.standard.synchronize()
                            }
                            let fresult = filteredResults.reversed()
                            
                            for obj in fresult{
                                if obj.product_name == "Commercial Vehicle"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("0~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(5)")
                                        UserDefaults.standard.synchronize()
                                    }else if obj.expire_date == "" {
                                        UserDefaults.standard.removeObject(forKey: "quiz\(5)")
                                        UserDefaults.standard.synchronize()
                                    }else{
                                        print("0~~~~1")
                                        UserDefaults.standard.set(1, forKey: "quiz\(5)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "General Knowledge"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("1~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(3)")
                                        UserDefaults.standard.synchronize()
                                    }else if obj.expire_date == "" {
                                        UserDefaults.standard.removeObject(forKey: "quiz\(3)")
                                        UserDefaults.standard.synchronize()
                                    }else{
                                        print("1~~~~1")
                                        UserDefaults.standard.set(1, forKey: "quiz\(3)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "Full Key Package"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("2~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(0)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(2)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(3)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(4)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(5)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(6)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(7)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(8)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(9)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(10)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(11)")
                                    }
                                }else if obj.product_name == "Permit Pass Package"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("3~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(1)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(2)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(3)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(4)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(5)")
//                                        UserDefaults.standard.removeObject(forKey: "quiz\(6)")
                                        UserDefaults.standard.synchronize()
                                    }
                                    
                                    
                                    
                                    
                                }else if obj.product_name == "Pre-Trip Inspection"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("4~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(4)")
                                        UserDefaults.standard.synchronize()
                                    }else if obj.expire_date == "" {
                                        UserDefaults.standard.removeObject(forKey: "quiz\(4)")
                                        UserDefaults.standard.synchronize()
                                    }else{
                                        print("4~~~~1",obj.expire_date)
                                        UserDefaults.standard.set(1, forKey: "quiz\(4)")
                                        //alreadyPurchasedQuiz.append(4)
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "Air Brake Endorsement"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("5~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(6)")
                                        UserDefaults.standard.synchronize()
                                    }else if obj.expire_date == "" {
                                        UserDefaults.standard.removeObject(forKey: "quiz\(6)")
                                        UserDefaults.standard.synchronize()
                                    }else{
                                        print("5~~~1")
                                        UserDefaults.standard.set(1, forKey: "quiz\(6)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "Combination"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("6~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(2)")
                                        UserDefaults.standard.synchronize()
                                    }else if obj.expire_date == "" {
                                        UserDefaults.standard.removeObject(forKey: "quiz\(2)")
                                        UserDefaults.standard.synchronize()
                                    }else{
                                        print("6~~~~1",obj.expire_date)
                                        
                                        UserDefaults.standard.set(1, forKey: "quiz\(2)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "Hazmat Endorsement"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("7~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(7)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "Tanker Endorsement"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("8~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(8)")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(8)_expDate")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "Double / Triples Endorsement"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    print("9~~~~0",obj.expire_date)
                                    if Date() > exp_date ?? Date(){
                                        print("9~~~~1",obj.expire_date)
                                        UserDefaults.standard.removeObject(forKey: "quiz\(10)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "School Bus Endorsement"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("10~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(11)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }else if obj.product_name == "Passenger Endorsement"{
                                    let exp_date = formatter.date(from: obj.expire_date ?? "")
                                    if Date() > exp_date ?? Date(){
                                        print("11~~~~")
                                        UserDefaults.standard.removeObject(forKey: "quiz\(9)")
                                        UserDefaults.standard.synchronize()
                                    }
                                }
                                
                            }
                            selectQuizViewController.setArrays()
                            if selectQuizViewController.collectionview != nil{
                                selectQuizViewController.collectionview.reloadData()
                            }
                            
                        }
                    }else{
                        print("objCommonResponse.message")
                        var isPlaced = false
                        isDataGetEmpty = false
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        let CVExpires_date = UserDefaults.standard.value(forKey: "CVExpires_date") as? String ?? ""
                        if CVExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= CVExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "commercial-vehicle", productExpiry: "\(CVExpires_date)")
                            }
                        }
                        let GKExpires_date = UserDefaults.standard.value(forKey: "GKExpires_date") as? String ?? ""
                        if GKExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= GKExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "general-knowledge", productExpiry: "\(GKExpires_date)")
                            }
                        }
                        
                        let FKPExpires_date = UserDefaults.standard.value(forKey: "FKPExpires_date") as? String ?? ""
                        if FKPExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= FKPExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "full-key", productExpiry: "\(FKPExpires_date)")
                            }
                        }
                        let PPPExpires_date = UserDefaults.standard.value(forKey: "PPPExpires_date") as? String ?? ""
                        if PPPExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= PPPExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "permit-pass", productExpiry: "\(PPPExpires_date)")
                            }
                        }
                        
                        let PIExpires_date = UserDefaults.standard.value(forKey: "PIExpires_date") as? String ?? ""
                        if PIExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= PIExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "pre-trip", productExpiry: "\(PIExpires_date)")
                            }
                        }
                        
                        let AEExpires_date = UserDefaults.standard.value(forKey: "AEExpires_date") as? String ?? ""
                        if AEExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= AEExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "air-brake", productExpiry: "\(AEExpires_date)")
                            }
                        }
                        
                        let CKPExpires_date = UserDefaults.standard.value(forKey: "CKPExpires_date") as? String ?? ""
                        if CKPExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= CKPExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "combination", productExpiry: "\(CKPExpires_date)")
                            }
                        }
                        
                        let HEExpires_date = UserDefaults.standard.value(forKey: "HEExpires_date") as? String ?? ""
                        if HEExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= HEExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "hazmat", productExpiry: "\(HEExpires_date)")
                            }
                        }
                        
                        let TEExpires_date = UserDefaults.standard.value(forKey: "TEExpires_date") as? String ?? ""
                        if TEExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= TEExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "tanker", productExpiry: "\(TEExpires_date)")
                            }
                        }
                        
                        let DTExpires_date = UserDefaults.standard.value(forKey: "DTExpires_date") as? String ?? ""
                        if DTExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= DTExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "doubles-triples", productExpiry: "\(DTExpires_date)")
                            }
                        }
                        
                        let SBExpires_date = UserDefaults.standard.value(forKey: "SBExpires_date") as? String ?? ""
                        if SBExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= SBExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "school-bus", productExpiry: "\(SBExpires_date)")
                            }
                        }
                        
                        let PBExpires_date = UserDefaults.standard.value(forKey: "PBExpires_date") as? String ?? ""
                        if PBExpires_date != ""{
                            let cDate = "\(formatter.string(from: Date()))"
                            if cDate <= PBExpires_date{
                                isPlaced = true
                                selectQuizViewController.updateExpireDate(productTag: "passenger", productExpiry: "\(PBExpires_date)")
                            }
                        }
                        if isPlaced == true{
                            self.getAllOrder()
                            selectQuizViewController.setArrays()
                            if selectQuizViewController.collectionview != nil{
                                selectQuizViewController.collectionview.reloadData()
                            }
                        }
                        
                    }
                    //self.syncOrders(alreadyPurchasedQuiz)
                }
                LoaderController.sharedInstance.removeLoader()
            }
        })
    }
    
    func saveNewDate(newDate: String, quizNumber: Int) -> Bool{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        print(quizNumber, newDate)
        if let syncExpDate = formatter.date(from: newDate), let savedExpDate = formatter.date(from: (UserDefaults.standard.value(forKey: "quiz\(quizNumber)_expDate") as? String ?? "")){
            print("saveNewDate-syncExpDate~~\(syncExpDate)~savedExpDate~~\(savedExpDate)")
            if savedExpDate < syncExpDate{ // repurchased a quiz
                print("savedExpDate < syncExpDate")
                return false
            }
            if savedExpDate > syncExpDate{ // repurchased a quiz
                print("savedExpDate > syncExpDate")
                return true
            }
//            if savedExpDate < syncExpDate{ // repurchased a quiz
//                return true
//            }
        }else if let syncExpDate = formatter.date(from: newDate){ // if quiz exp date not present in device and expired date coming from api is really expired, then remove quiz from device, ask to repurchase
            print("syncExpDate~~\(syncExpDate)")
            if Date() > syncExpDate{
                UserDefaults.standard.removeObject(forKey: "quiz\(quizNumber)")
                return false
            }
        }
        return true
    }
    
    func isExpired(_ quizNumber: Int) -> Bool{
        guard let expDateStr = UserDefaults.standard.value(forKey: "quiz\(quizNumber)_expDate") as? String else {
            return false
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let expDate = formatter.date(from: expDateStr){
            if Date() > expDate{
                return true
            }
        }
        return false
    }
    
    func syncOrders(_ alreadyPurchasedQuiz: [Int]){
        
        //            user_id = 117 (value)
        //            product_tag  =    (commercial-vehicle, general-knowledge, full-key, permit-pass, pre-trip, air-brake, combination, hazmat, tanker, doubles-triples, school-bus, passenger)
        var productTag = ""
        var productExpiry = ""
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        for i in 0..<12{
            print("i - ",i)
            if isKeyPresentInUserDefaults(key: "quiz\(i)"){
                print("i ======= quiz\(i)")
                var isExpiredQuiz = false
                let expDateStr = UserDefaults.standard.value(forKey: "quiz\(i)_expDate") as? String ?? ""
                if let expDate = formatter.date(from: expDateStr){
                    if Date() > expDate{
                        isExpiredQuiz = true
                    }
                }
                
                print("isExpiredQuiz=\(isExpiredQuiz),alreadyPurchasedQuiz\(alreadyPurchasedQuiz)")
                if (i == 0 && isExpiredQuiz) || (i == 0 && !alreadyPurchasedQuiz.contains(0)){
                    productTag += "full-key,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 1 && isExpiredQuiz) || (i == 1 && !alreadyPurchasedQuiz.contains(1)){
                    productTag += "permit-pass,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 2 && isExpiredQuiz) || (i == 2 && !alreadyPurchasedQuiz.contains(2)){
                    productTag += "combination,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 3 && isExpiredQuiz) || (i == 3 && !alreadyPurchasedQuiz.contains(3)){
                    productTag += "general-knowledge,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 4 && isExpiredQuiz) || (i == 4 && !alreadyPurchasedQuiz.contains(4)){
                    productTag += "pre-trip,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 5 && isExpiredQuiz) || (i == 5 && !alreadyPurchasedQuiz.contains(5)){
                    productTag += "commercial-vehicle,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 6 && isExpiredQuiz) || (i == 6 && !alreadyPurchasedQuiz.contains(6)){
                    productTag += "air-brake,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 7 && isExpiredQuiz) || (i == 7 && !alreadyPurchasedQuiz.contains(7)){
                    productTag += "hazmat,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 8 && isExpiredQuiz) || (i == 8 && !alreadyPurchasedQuiz.contains(8)){
                    productTag += "tanker,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 9 && isExpiredQuiz) || (i == 9 && !alreadyPurchasedQuiz.contains(9)){
                    productTag += "passenger,"
                    print("quiz\(i)_expDate",UserDefaults.standard.value(forKey: "quiz\(i)_expDate"))
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 10 && isExpiredQuiz) || (i == 10 && !alreadyPurchasedQuiz.contains(10)){
                    productTag += "doubles-triples,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }else if (i == 11 && isExpiredQuiz) || (i == 11 && !alreadyPurchasedQuiz.contains(11)){
                    productTag += "school-bus,"
                    productExpiry += "\(UserDefaults.standard.value(forKey: "quiz\(i)_expDate") ?? ""),"
                }
            }
        }
        if productExpiry.count > 0{
            productExpiry.removeLast()
        }
        if productTag.count > 0{
            productTag.removeLast()
        }else{
//            showAlertView(title: "Alert", message: "All purchases are already synced")//All purchases are already synced or No purchase found-expirydatedata
            
            print("ordering ---\(productTag.count)--- product_tag: \(productTag) productExpiry: \(productExpiry)")
            return;
        }
//        if productTag.count > 0{
//            print("ordering ------1 product_tag: \(productTag) productExpiry: \(productExpiry)")
//            fetchGenericData(urlString: "order_place", parameters: "user_id=\(UserDefaults.standard.value(forKey: "userId")!)&product_tag=\(productTag)&expire_date=\(productExpiry)", method: "POST", completion: {(objCommonResponse: AllOrdersLogin?, str) in
//                DispatchQueue.main.async {
//                    if let objCommonResponse = objCommonResponse{
//                        if objCommonResponse.status {
//                            self.showAlertView(title: "Alert", message: "Synced successfully")
//                        }else{
//                            self.showAlertView(title: "Alert", message: objCommonResponse.message)
//                        }
//                    }
//                    LoaderController.sharedInstance.removeLoader()
//                }
//            })
//        }
    }
}
