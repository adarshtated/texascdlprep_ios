//
//  ResultViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 04/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, XMLParserDelegate{
    var array_correct_answers = NSMutableArray()
    var array_answers = NSMutableArray()
    var finalarr = NSMutableArray()
    var percentage = Int()
    var array_lbl_qno = [UILabel]()
    var array_lbl_question = [UILabel]()
    var array_lbl_youanswered = [UILabel]()
    var array_lbl_correctanswer = [UILabel]()
    var arr = [String]()
    var count5 = 1
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var lbl_score: UILabel!
    @IBOutlet var lbl_individualscore: UILabel!
    @IBOutlet var btn_startquiz: UIButton!
    var imageString = String()
    var showSavedResult = Int()
    var quiz_number = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.setImage(UIImage(named:"back_menu.png"), for: .normal)
        menuBtn.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 25)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 25)
        currHeight?.isActive = true
        self.navigationItem.leftBarButtonItem = menuBarItem
//        setUpRightMenuButton()
        print("imageString - - - -",imageString,"\(imageString).png")
        btn_startquiz.setImage(UIImage(named: "\(imageString).png"), for: .normal)
        self.navigationController?.navigationBar.isHidden = true
//        print("showSavedResult --",showSavedResult)
        lbl_score.text = "Score: \(percentage)%"
      
        
        if showSavedResult == 1{
            if let arr = UserDefaults.standard.array(forKey: "quiz_no\(quiz_number)_array_answers"){
                finalarr = (UserDefaults.standard.array(forKey: "quiz_no\(quiz_number)_finalarr")! as NSArray).mutableCopy() as! NSMutableArray
                
                for i in 0..<finalarr.count{
                    let arr = finalarr[i] as? NSArray
                    array_correct_answers.add(arr![4])
                }
                
                array_answers = (arr as NSArray).mutableCopy() as! NSMutableArray// as! [String]
            }else{
                print("empty answer array")
                for _ in 0..<finalarr.count{
                    array_answers.add("dontKnow".localized())
                }
            }
            if let arr = UserDefaults.standard.value(forKey: "quiz_no\(quiz_number)_high_score"){
                percentage = arr as! Int
            }
        }else{
            UserDefaults.standard.set(array_answers, forKey: "quiz_no\(quiz_number)_array_answers")
            UserDefaults.standard.set(finalarr, forKey: "quiz_no\(quiz_number)_finalarr")
            // high score logic
            if let preview_highscore = UserDefaults.standard.value(forKey: "quiz_no\(quiz_number)_high_score"){
                if percentage > preview_highscore as! Int{
                    UserDefaults.standard.set(percentage, forKey: "quiz_no\(quiz_number)_high_score")
                }
            }else{
                UserDefaults.standard.set(percentage, forKey: "quiz_no\(quiz_number)_high_score")
            }
            UserDefaults.standard.synchronize()
        }
        print("final count",finalarr.count)
        print("correct answer count", array_correct_answers.count)
        print("array_answers --- ",array_answers)
        setupView()
    }
    
    @objc func goBack(){
        print("handle menu")
        self.navigationController?.popViewController(animated: true)
    }
    
    
    fileprivate func setupView(){
        let firstlbl = UILabel()
        scrollview.addSubview(firstlbl)
        firstlbl.text = "Question 1"
        firstlbl.font = firstlbl.font.withSize(25)
        firstlbl.topAnchor.constraint(equalTo: lbl_individualscore.bottomAnchor, constant: 30).isActive = true
        firstlbl.leadingAnchor.constraint(equalTo: scrollview.leadingAnchor, constant: 20).isActive = true
        firstlbl.widthAnchor.constraint(equalToConstant: 200).isActive = true
        firstlbl.numberOfLines = 0
        firstlbl.translatesAutoresizingMaskIntoConstraints = false
        array_lbl_qno.append(firstlbl)
        
        for i in 0..<finalarr.count{
        
//            print("i---",i)
            let arr = finalarr[i] as? NSArray
            
            let lbl_question_number = UILabel()
            let lbl_question = UILabel()
            let lbl_youanswered = UILabel()
            let lbl_correctanswer = UILabel()
            
            scrollview.addSubview(lbl_question_number)
            scrollview.addSubview(lbl_question)
            scrollview.addSubview(lbl_youanswered)
            scrollview.addSubview(lbl_correctanswer)
            
            lbl_question.topAnchor.constraint(equalTo: array_lbl_qno[i].bottomAnchor, constant: 20).isActive = true
            lbl_question.leadingAnchor.constraint(equalTo: scrollview.leadingAnchor, constant: 20).isActive = true
            lbl_question.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 40).isActive = true
            lbl_question.translatesAutoresizingMaskIntoConstraints = false
            lbl_question.text = arr![0] as? String
            lbl_question.numberOfLines = 0
            lbl_question.textColor = UIColor.gray
            array_lbl_question.append(lbl_question)
            
            lbl_youanswered.topAnchor.constraint(equalTo: array_lbl_question[i].bottomAnchor, constant: 10).isActive = true
            lbl_youanswered.leadingAnchor.constraint(equalTo: scrollview.leadingAnchor, constant: 20).isActive = true
            lbl_youanswered.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 40).isActive = true
            lbl_youanswered.numberOfLines = 0
            lbl_youanswered.translatesAutoresizingMaskIntoConstraints = false
            lbl_youanswered.textColor = UIColor.gray
            var ans = array_answers[i] as! String
            if ans == "opt1"{
                ans = arr![1] as! String
            }else if ans == "opt2"{
                ans = arr![2] as! String
            }else if ans == "opt3"{
                ans = arr![3] as! String
            }else{
                ans = "dontKnow".localized()
            }
            lbl_youanswered.text = "youAnswered".localized() + "\(ans)"
            array_lbl_youanswered.append(lbl_youanswered)
            
            lbl_correctanswer.topAnchor.constraint(equalTo: array_lbl_youanswered[i].bottomAnchor, constant: 10).isActive = true
            lbl_correctanswer.leadingAnchor.constraint(equalTo: scrollview.leadingAnchor, constant: 20).isActive = true
            lbl_correctanswer.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 40).isActive = true
            lbl_correctanswer.numberOfLines = 0
            lbl_correctanswer.translatesAutoresizingMaskIntoConstraints = false
            lbl_correctanswer.textColor = UIColor.gray
            ans = array_correct_answers[i] as! String 
            if ans == "opt1"{
                ans = arr![1] as! String
            }else if ans == "opt2"{
                ans = arr![2] as! String
            }else if ans == "opt3"{
                ans = arr![3] as! String
            }else{
                ans = "dontKnow".localized()
            }
            lbl_correctanswer.text = "incorrect".localized() + "\(ans)"
            array_lbl_correctanswer.append(lbl_correctanswer)
            
            lbl_question_number.topAnchor.constraint(equalTo: array_lbl_correctanswer[i].bottomAnchor, constant: 20).isActive = true
            lbl_question_number.leadingAnchor.constraint(equalTo: scrollview.leadingAnchor, constant: 20).isActive = true
            lbl_question_number.widthAnchor.constraint(equalToConstant: 200).isActive = true
            lbl_question_number.numberOfLines = 0
            lbl_question_number.translatesAutoresizingMaskIntoConstraints = false
            lbl_question_number.text = "Question \(i + 2)"
            lbl_question_number.font = lbl_question_number.font.withSize(25)
            array_lbl_qno.append(lbl_question_number)
        }
        array_lbl_qno[array_lbl_qno.count - 1].removeFromSuperview()
        array_lbl_qno.removeLast()
        scrollview.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        scrollview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        scrollview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        scrollview.bottomAnchor.constraint(equalTo: (array_lbl_correctanswer[array_lbl_correctanswer.count - 1]).bottomAnchor, constant: 10).isActive = true
        scrollview.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.8).isActive = true
        scrollview.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @IBAction func showSelectQuizPage(_ sender: UIButton) {
            //show select quiz with prize
        self.navigationController?.navigationBar.isHidden = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SelectQuizViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                return
            }
        }
    }
    
    @IBAction func showStartQuizPage(_ sender: UIButton) {
        print("push start quiz vc")
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: StartQuizViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                return
            }
        }
    }
}
