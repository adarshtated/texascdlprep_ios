//
//  StartQuizViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class StartQuizViewController: UIViewController {
    var quiz_number = Int()
    
    @IBOutlet var imgview: UIImageView!
    @IBOutlet var lbl_highscore: UILabel!
    var array_quiz1 = ["Combination Vehicle","General Knowledge","Pre- Trip Inspection","Commercial Vehicle","Air Brakes Endorsements","Hazmat Endorsement","Tanker Endorsement","Passenger Endorsement","Double Endorsement","School Bus Endorsement"]
    var array_image1 = ["combo1256","books256","inspect128","truck128","brakes128","haz128","tanker128","passenger128","double128","school128"]
   
    override func viewDidLoad() {
        super.viewDidLoad()
//        setUpMenuButton()
//        setUpRightMenuButton()
//        Common.objVC = self
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)

        
        imgview.image = UIImage(named: array_image1[quiz_number])
        imgview.contentMode = .scaleAspectFit
        
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    
    @IBAction func reset_highscore(_ sender: UIButton) {
        let alertController = UIAlertController(title: "resetHighscoreBtn".localized(), message: "txHighConfirmMessage".localized(), preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: reset)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(OKAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        changeLanguage()
    }
    
    func changeLanguage(){
        print("startquizviewcontroller")
        switch array_quiz1[quiz_number]{
        case "Full Key Package":
            self.title = "app_selectionfull".localized().capitalized
        case "Permit Pass Package":
            self.title = "app_selectionpermit".localized().capitalized
        case "Combination Vehicle":
            self.title = "app_combo".localized().capitalized
        case "General Knowledge":
            self.title = "app_general".localized().capitalized
        case "Pre- Trip Inspection":
            self.title = "app_pretrip".localized().capitalized
        case "Commercial Vehicle":
            self.title = "app_txcomm".localized().capitalized
        case "Air Brakes Endorsements":
            self.title = "app_airbrakes".localized().capitalized
        case "Hazmat Endorsement":
            self.title = "app_hazmat".localized().capitalized
        case "Tanker Endorsement":
            self.title = "app_tanker".localized().capitalized
        case "Passenger Endorsement":
            self.title = "app_passenger".localized().capitalized
        case "Double Endorsement":
            self.title = "app_double".localized().capitalized
        case "School Bus Endorsement":
            self.title = "app_school".localized().capitalized
        default:
            break;
        }
        if let highscore = UserDefaults.standard.value(forKey: "quiz_no\(quiz_number)_high_score") as? Int{
            lbl_highscore.text = "highscore".localized()+"\(highscore)%"
        }else{
            lbl_highscore.text = "highscore".localized() + "0%"
            print("no previous highscore")
        }
    }
    
    func reset(_:UIAlertAction){
        UserDefaults.standard.set(0, forKey: "quiz_no\(quiz_number)_high_score")
        UserDefaults.standard.synchronize()
        lbl_highscore.text = "highscore".localized() + "0%"
    }
    
    @IBAction func showResultPage(_ sender: UIButton) {
        if UserDefaults.standard.array(forKey: "quiz_no\(quiz_number)_array_answers") == nil{
             let ac = UIAlertController(title: "Alert", message: "No saved result! Please take a quiz first", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(ac, animated: false, completion: nil)
        }else{
            let next = storyboard?.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
            next.quiz_number = quiz_number
            next.showSavedResult = 1
            next.imageString = array_image1[quiz_number]
            self.navigationController?.pushViewController(next, animated: true)
        }
    }
    
    @IBAction func startQuiz(_ sender: UIButton) {
        let next = storyboard?.instantiateViewController(withIdentifier: "QuizViewController") as! QuizViewController
        next.quiz_number = self.quiz_number
        next.imgstring = array_image1[quiz_number]
        self.navigationController?.pushViewController(next, animated: true)
    }
}
