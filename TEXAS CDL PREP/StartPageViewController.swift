//
//  StartPageViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 02/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class StartPageViewController: UIViewController {
    let innerPulsatingLayer = CAShapeLayer()
    let pulsatingLayer = CAShapeLayer()
    let Rectangle = UIImageView()
    let backview = UIView()
    
    @IBOutlet var btn_getStarted: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.objVC = self
        IAPHandler.shared.fetchAvailableProducts()
        //selectQuizViewController.setArrays()
        
        
        backview.frame = CGRect(x: 0, y:  UIScreen.main.bounds.height * 0.5, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        btn_getStarted.frame = CGRect(x: 0, y:  UIScreen.main.bounds.height * 0.8, width: UIScreen.main.bounds.width, height: 70)
        btn_getStarted.layer.cornerRadius = 35
        Rectangle.frame = CGRect(x: UIScreen.main.bounds.midX, y: (UIScreen.main.bounds.height * 0.8)+10, width: 0, height: 0)
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 30, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        let innerCircularPath = UIBezierPath(arcCenter: .zero, radius: 30, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        Rectangle.backgroundColor = UIColor(displayP3Red: 44/255, green: 91/255, blue: 137/255, alpha: 1)
        
        setUpMenuButton()
        setUpRightMenuButton()
        btn_getStarted.setTitle("start_button".localized(), for: .normal)
        
        innerPulsatingLayer.path = innerCircularPath.cgPath
        innerPulsatingLayer.strokeColor = UIColor.clear.cgColor
        innerPulsatingLayer.lineWidth = 10
        innerPulsatingLayer.fillColor = UIColor.white.cgColor
        innerPulsatingLayer.lineCap = CAShapeLayerLineCap.round
        innerPulsatingLayer.position = view.center
        
        pulsatingLayer.path = circularPath.cgPath
        pulsatingLayer.strokeColor = UIColor.clear.cgColor
        pulsatingLayer.lineWidth = 10
        pulsatingLayer.fillColor = UIColor.red.cgColor
        pulsatingLayer.lineCap = CAShapeLayerLineCap.round
        pulsatingLayer.position = view.center
        
        if !isKeyPresentInUserDefaults(key: "userId"){
            let alertController = UIAlertController(title: "Alert", message: "Please do login or registration to sync your details to other devices.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Register", style: .default, handler: { _ in
//                let _ = RegisterView(frame: self.view.frame, viewController: self, topView: self.view)
                let next = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                //next.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(next, animated: true)
            }))
            alertController.addAction(UIAlertAction(title: "Login", style: .default, handler: { _ in
//                let _ = LoginView(frame: self.view.frame, viewController: self, topView: self.view)
                let next = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                //next.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(next, animated: true)
                
            }))
            present(alertController, animated: false)
            UserDefaults.standard.synchronize()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isDataGetEmpty = false
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        isDataGetEmpty = false
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)

        if isKeyPresentInUserDefaults(key: "userId"){
            IAPHandler.shared.tryCheckValidateReceiptAndUpdateExpirationDate()
        }
        
    }
    
    
    private func animateButton(){
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            self.btn_getStarted.frame = CGRect(x: self.view.bounds.midX - 20, y:  UIScreen.main.bounds.height * 0.8, width: 40, height: 40)
            self.btn_getStarted.setTitle("", for: .normal)
            self.btn_getStarted.layer.cornerRadius = 20
        }, completion: {finished in
            print("animate button1")
        })
    }
    
    func changeLanguage(){
        btn_getStarted.setTitle("start_button".localized(), for: .normal)
    }
    
    private func animateButton2(){
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseIn, animations: {
            self.btn_getStarted.frame = CGRect(x: 0, y:  UIScreen.main.bounds.height * 0.8, width: UIScreen.main.bounds.width, height: 70)
            self.btn_getStarted.layer.cornerRadius = 35
        }, completion: {finished in
            print("animate button2")
            let next = self.storyboard?.instantiateViewController(withIdentifier: "SelectQuizViewController") as! SelectQuizViewController
            self.navigationController?.pushViewController(next, animated: false)
        })
    }
    
    fileprivate func animateBlueRectangle(){
        UIView.animate(withDuration: 0.3, delay: 1, options: .curveEaseIn, animations: {
            self.Rectangle.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }, completion: {finished in
//            print("animate blue rectangle")
            self.view.addSubview(self.backview)
            self.backview.layer.addSublayer(self.innerPulsatingLayer)
            self.backview.layer.addSublayer(self.pulsatingLayer)
            self.animatePulsatingLayer()
            self.animateInnerPulsatingLayer()
        })
    }
    
    fileprivate func reduceBlueRectangle() {
        UIView.animate(withDuration: 0.5, delay: 6, options: .curveEaseIn, animations: {
            self.Rectangle.frame = CGRect(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.height * 0.8, width: 0, height: 0)
            self.innerPulsatingLayer.frame = CGRect(x: UIScreen.main.bounds.width * 0.5, y: UIScreen.main.bounds.height * 0.7 * 0.5, width: 0, height: 0)
            self.pulsatingLayer.frame = CGRect(x: UIScreen.main.bounds.width * 0.5, y: UIScreen.main.bounds.height * 0.7 * 0.5, width: 0, height: 0)
        }, completion: {finished in
            self.btn_getStarted.setTitle("start_button".localized(), for: .normal)
//            print("finish with blue rectangle")
        })
    }
    
    private func animatePulsatingLayer(){
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.5
        animation.duration = 0.5
        animation.autoreverses = true
        animation.repeatCount = 7
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    
    private func animateInnerPulsatingLayer(){
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 2.3
        animation.duration = 0.5
        animation.autoreverses = true
        animation.repeatCount = 7
        innerPulsatingLayer.add(animation, forKey: "pulsing")
    }
    
    @IBAction func startTransform(_ sender: Any) {
        if !isKeyPresentInUserDefaults(key: "userId"){
            let alertController = UIAlertController(title: "Alert", message: "Please do login or registration to sync your details to other devices.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Register", style: .default, handler: { _ in
//                let _ = RegisterView(frame: self.view.frame, viewController: self, topView: self.view)
                let next = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                //next.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(next, animated: true)
            }))
            alertController.addAction(UIAlertAction(title: "Login", style: .default, handler: { _ in
                //let _ = LoginView(frame: self.view.frame, viewController: self, topView: self.view)
                let next = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                //next.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(next, animated: true)
                
            }))
            present(alertController, animated: false)
            UserDefaults.standard.synchronize()
            return
        }
        btn_getStarted.setTitle("", for: .normal)
        view.addSubview(Rectangle)
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            self.innerPulsatingLayer.removeFromSuperlayer()
            self.pulsatingLayer.removeFromSuperlayer()
            self.Rectangle.removeFromSuperview()
            self.animateButton2()
            self.backview.removeFromSuperview()
            self.Rectangle.removeFromSuperview()
            print("pulsating done")
        })
        self.animateButton()
        self.animateBlueRectangle()
        self.reduceBlueRectangle()
        CATransaction.commit()
    }
}


