//
//  QuizViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Foundation

class QuizViewController: UIViewController, XMLParserDelegate {

    @IBOutlet var lbl_question_count: UILabel!
    @IBOutlet var lbl_question: UILabel!
    @IBOutlet var lbl_option1: UILabel!
    @IBOutlet var lbl_option2: UILabel!
    @IBOutlet var lbl_option3: UILabel!
    @IBOutlet var lbl_option4: UILabel!
    @IBOutlet var btn_option1: UIButton!
    @IBOutlet var btn_option2: UIButton!
    @IBOutlet var btn_option3: UIButton!
    @IBOutlet var btn_option4: UIButton!
    @IBOutlet var btn_next: UIButton!
    @IBOutlet var btn_previous: UIButton!
    @IBOutlet var btn_back: UIButton!
    @IBOutlet var btn_save: UIButton!
    
    var array_image1 = ["combo1256","books256","inspect128","truck128","brakes128","haz128","tanker128","passenger128","double128","school128"]
    var array_file = [String]()
    var array_correct_answers = NSMutableArray()
    var array_answers = NSMutableArray()
    var finalarr = NSMutableArray()
    var arr = [String]()
    var count5 = 0
    var quiz_number = Int()
    var current_question = 0
    var scrollview = UIScrollView()
    var view0 = UIView()
    var imgstring = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.value(forKey: "isEnglish") as! Bool{
            array_file = ["comquestions","genquestions","pretripquestions","texascomquestions","airquestions","hazquestions","tankerqanda","passengerqanda","doublesqanda","schoolqanda"]
        }else{
            print("spanish txt file")
            array_file = ["comquestions-es","genquestions-es","pretripquestions-es","texascomquestions-es","airquestions-es","hazquestions-es","tankerqanda-es","passengerqanda-es","doublesqanda-es","schoolqanda-es"]
        }
        
        self.navigationController?.navigationBar.isHidden = true
        btn_option1.setDeselected()
        btn_option2.setDeselected()
        btn_option3.setDeselected()
        btn_option4.setSelected()
        lbl_option1.font = lbl_option1.font.withSize(15)
        lbl_option2.font = lbl_option1.font.withSize(15)
        lbl_option3.font = lbl_option1.font.withSize(15)
        lbl_option4.font = lbl_option1.font.withSize(15)
        
        self.view.addSubview(scrollview)
        
        scrollview.addSubview(lbl_question_count)
        scrollview.addSubview(lbl_question)
        scrollview.addSubview(btn_option1)
        scrollview.addSubview(btn_option2)
        scrollview.addSubview(btn_option3)
        scrollview.addSubview(btn_option4)
        scrollview.addSubview(lbl_option1)
        scrollview.addSubview(lbl_option2)
        scrollview.addSubview(lbl_option3)
        scrollview.addSubview(lbl_option4)
        scrollview.addSubview(btn_previous)
        scrollview.addSubview(btn_next)
        
        btn_back.setImage(UIImage(named: imgstring), for: .normal)
        
        lbl_question_count.topAnchor.constraint(equalTo: self.scrollview.topAnchor, constant: 50).isActive = true
        lbl_question_count.leftAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: 10).isActive = true
        lbl_question_count.rightAnchor.constraint(equalTo: self.scrollview.rightAnchor, constant: -10).isActive = true
        lbl_question_count.heightAnchor.constraint(equalToConstant: 50)
        lbl_question_count.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl_question.topAnchor.constraint(equalTo: lbl_question_count.bottomAnchor, constant: 30).isActive = true
        lbl_question.leftAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: 10).isActive = true
        lbl_question.widthAnchor.constraint(equalToConstant: self.view.bounds.width - 20).isActive = true
        lbl_question.numberOfLines = 0
        lbl_question.sizeToFit()
        lbl_question.translatesAutoresizingMaskIntoConstraints = false
        
        
        btn_option1.topAnchor.constraint(equalTo: lbl_question.bottomAnchor, constant: 30).isActive = true
        btn_option1.leftAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: self.view.bounds.width * 0.13).isActive = true
        btn_option1.widthAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option1.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option1.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl_option1.topAnchor.constraint(equalTo: lbl_question.bottomAnchor, constant: 30).isActive = true
        lbl_option1.leftAnchor.constraint(equalTo: btn_option1.rightAnchor, constant: 10).isActive = true
        lbl_option1.widthAnchor.constraint(equalToConstant: self.view.bounds.width * 0.6).isActive = true
        lbl_option1.numberOfLines = 0
        lbl_option1.sizeToFit()
        lbl_option1.translatesAutoresizingMaskIntoConstraints = false
        
        
        btn_option2.topAnchor.constraint(equalTo: lbl_option1.bottomAnchor, constant: 15).isActive = true
        btn_option2.leftAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: self.view.bounds.width * 0.13).isActive = true
        btn_option2.widthAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option2.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl_option2.topAnchor.constraint(equalTo: lbl_option1.bottomAnchor, constant: 15).isActive = true
        lbl_option2.leftAnchor.constraint(equalTo: btn_option2.rightAnchor, constant: 10).isActive = true
        lbl_option2.widthAnchor.constraint(equalToConstant: self.view.bounds.width * 0.65).isActive = true
        lbl_option2.numberOfLines = 0
        lbl_option2.sizeToFit()
        lbl_option2.translatesAutoresizingMaskIntoConstraints = false
        
        
        btn_option3.topAnchor.constraint(equalTo: lbl_option2.bottomAnchor, constant: 15).isActive = true
        btn_option3.leftAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: self.view.bounds.width * 0.13).isActive = true
        btn_option3.widthAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option3.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option3.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl_option3.topAnchor.constraint(equalTo: lbl_option2.bottomAnchor, constant: 15).isActive = true
        lbl_option3.leftAnchor.constraint(equalTo: btn_option2.rightAnchor, constant: 10).isActive = true
        lbl_option3.widthAnchor.constraint(equalToConstant: self.view.bounds.width * 0.65).isActive = true
        lbl_option3.numberOfLines = 0
        lbl_option3.sizeToFit()
        lbl_option3.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        btn_option4.topAnchor.constraint(equalTo: lbl_option3.bottomAnchor, constant: 15).isActive = true
        btn_option4.leftAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: self.view.bounds.width * 0.13).isActive = true
        btn_option4.widthAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option4.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btn_option4.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl_option4.topAnchor.constraint(equalTo: lbl_option3.bottomAnchor, constant: 15).isActive = true
        lbl_option4.leftAnchor.constraint(equalTo: btn_option3.rightAnchor, constant: 10).isActive = true
        lbl_option4.widthAnchor.constraint(equalToConstant: self.view.bounds.width * 0.65).isActive = true
        lbl_option4.numberOfLines = 0
        lbl_option4.sizeToFit()
        lbl_option4.translatesAutoresizingMaskIntoConstraints = false
        
        
        btn_previous.topAnchor.constraint(equalTo: lbl_option4.bottomAnchor, constant: 40).isActive = true
        btn_previous.rightAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: self.view.bounds.midX - 10).isActive = true
        btn_previous.widthAnchor.constraint(equalToConstant: 70).isActive = true
        btn_previous.heightAnchor.constraint(equalToConstant: 70).isActive = true
        btn_previous.translatesAutoresizingMaskIntoConstraints = false
        
        
        btn_next.topAnchor.constraint(equalTo: lbl_option4.bottomAnchor, constant: 40).isActive = true
        btn_next.leftAnchor.constraint(equalTo: self.scrollview.leftAnchor, constant: self.view.bounds.midX + 10).isActive = true
        btn_next.widthAnchor.constraint(equalToConstant: 70).isActive = true
        btn_next.heightAnchor.constraint(equalToConstant: 70).isActive = true
        btn_next.translatesAutoresizingMaskIntoConstraints = false
        
        
        scrollview.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        scrollview.bottomAnchor.constraint(equalTo: self.btn_next.bottomAnchor, constant: 10).isActive = true
        scrollview.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        scrollview.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: 0).isActive = true
        scrollview.heightAnchor.constraint(equalToConstant: self.view.bounds.height * 0.8).isActive = true
        scrollview.translatesAutoresizingMaskIntoConstraints = false

        
        btn_save.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30).isActive = true
        btn_save.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: self.view.bounds.midX + 10).isActive = true
        btn_save.widthAnchor.constraint(equalToConstant: 70).isActive = true
        btn_save.heightAnchor.constraint(equalToConstant: 70).isActive = true
        btn_save.translatesAutoresizingMaskIntoConstraints = false
        
        btn_back.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30).isActive = true
        btn_back.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: self.view.bounds.midX - 70).isActive = true
        btn_back.widthAnchor.constraint(equalToConstant: 70).isActive = true
        btn_back.heightAnchor.constraint(equalToConstant: 70).isActive = true
        btn_back.translatesAutoresizingMaskIntoConstraints = false
        
        if let path = Bundle.main.url(forResource: array_file[quiz_number], withExtension: "txt") {
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
        print("final count",finalarr.count)
        lbl_question_count.text = "Question 1/\(finalarr.count)"
        for i in 0..<finalarr.count{
            let arr = finalarr[i] as? NSArray
            array_correct_answers.add(arr![4])
            array_answers.add("")
        }
        setUpValues()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    fileprivate func setUpValues(){
        lbl_question_count.text = "knowquestionNumberSingle".localized() + "\(current_question + 1)/\(finalarr.count)"
        let arr = finalarr[current_question] as? NSArray
        lbl_question.text = arr![0] as? String
        lbl_option1.text = arr![1] as? String
        lbl_option2.text = arr![2] as? String
        lbl_option3.text = arr![3] as? String
        lbl_option4.text = "dontKnow".localized()
    }
    
    
    @IBAction func select_option1(_ sender: UIButton) {
        btn_option1.setSelected()
        btn_option2.setDeselected()
        btn_option3.setDeselected()
        btn_option4.setDeselected()
        array_answers[current_question] = "opt1"
    }
    
    @IBAction func select_option2(_ sender: UIButton) {
        btn_option2.setSelected()
        btn_option1.setDeselected()
        btn_option3.setDeselected()
        btn_option4.setDeselected()
        array_answers[current_question] = "opt2"
    }
    
    @IBAction func select_option3(_ sender: UIButton) {
        btn_option3.setSelected()
        btn_option2.setDeselected()
        btn_option1.setDeselected()
        btn_option4.setDeselected()
        array_answers[current_question] = "opt3"
    }
    
    @IBAction func select_option4(_ sender: UIButton) {
        btn_option4.setSelected()
        btn_option2.setDeselected()
        btn_option3.setDeselected()
        btn_option1.setDeselected()
        array_answers[current_question] = ""
    }
    
    
    @IBAction func next(_ sender: UIButton) {
        print(finalarr.count, current_question)
        btn_option1.setDeselected()
        btn_option2.setDeselected()
        btn_option3.setDeselected()
        btn_option4.setSelected()
        if finalarr.count == current_question + 1{
            print("end of questions")
        }else{
            current_question += 1
            setUpValues()
        }
    }
    
    @IBAction func previous(_ sender: UIButton) {
        if current_question == 0{
            print("already on first question")
        }else{
            current_question -= 1
            setUpValues()
        }
    }

    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == "string-array" {
        }
    }
    
    // 2 This method is sent by the parser object when the end tag of  "</book>" is encountered.
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "string-array" {
        }
    }
    // 3 Here the actual parsing is executed. The tags will be parsed and the corresponding properties will be initialized.
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (!data.isEmpty) {//
            print("string: \(data)")
            if data.first == "ñ" || data.first == "ó" || data.first == "á" || data.first == "é" || data.first == "í" || data.first == "ú" || data.first == "ü"{
                if arr.indices.contains(arr.count-1){
                    arr[arr.count-1].append(data)
                }else{
                    arr.append(data)
                    count5 += 1
                }
            }else{
                arr.append(data)
                count5 += 1
            }
            if count5 == 5{
                finalarr.add(arr)
                arr.removeAll()
                count5 = 0
            }//else{
//                count5 += 1
//            }
//            print("final array: ",finalarr)
        }
    }
    
    @IBAction func page_dismiss(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submit(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Confirm Submission", message: "Are you sure?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: submit)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(OKAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    func submit(_:UIAlertAction){
        var total_correct_answers = 0
        for i in 0..<array_correct_answers.count{
            if array_correct_answers[i] as! String == array_answers[i] as! String{
//                print("answer matched")
                total_correct_answers += 1
            }//else{
//                print("answer not matched")
//            }
        }
        print("total_correct_answers ",total_correct_answers, "array_correct_answers.count", array_correct_answers.count)
        print("percentage = ", (total_correct_answers * 100) / array_correct_answers.count)
        let next = storyboard?.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
        next.array_answers = array_answers //as! [String]
        next.array_correct_answers = array_correct_answers //as! [String]
        next.quiz_number = quiz_number
        next.percentage = (total_correct_answers * 100) / array_correct_answers.count
        next.imageString = array_image1[quiz_number]
        next.finalarr = finalarr
        
        self.navigationController?.pushViewController(next, animated: true)
    }
}

