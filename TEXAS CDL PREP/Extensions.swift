//
//  Extensions.swift
//  TEXAS CDL PREP
//
//  Created by anushka mishra on 04/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
extension String {
    func localized() -> String {
        var lang = String()
        UserDefaults.standard.value(forKey: "isEnglish") as? Bool ?? true ? (lang = "en") : (lang = "es-419")
        //        print("lang from userdefaults: ",UserDefaults.standard.value(forKey: "isEnglish") as! Bool)
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        //        print("bundle:\(bundle)")
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
extension UIViewController{
    func setUpMenuButton(){
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.setImage(UIImage(named:"menu.png"), for: .normal)
        menuBtn.addTarget(self, action: #selector(handleMenu), for: UIControl.Event.touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 25)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 25)
        currHeight?.isActive = true
        self.navigationItem.leftBarButtonItem = menuBarItem
    }
    
    @objc func handleMenu(){
        SideMenu.sharedInstance.showSideBar(self)
    }
    
    func setUpRightMenuButton(){
        let menuBtn2 = UIButton(type: .custom)
        menuBtn2.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn2.setImage(UIImage(named:"menu_white.png"), for: .normal)
        menuBtn2.addTarget(self, action: #selector(showtoprightmenubtn), for: .touchUpInside)
        let menuBarItem2 = UIBarButtonItem(customView: menuBtn2)
        let currWidth2 = menuBarItem2.customView?.widthAnchor.constraint(equalToConstant: 25)
        currWidth2?.isActive = true
        let currHeight2 = menuBarItem2.customView?.heightAnchor.constraint(equalToConstant: 25)
        currHeight2?.isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem2
    }
    
    @objc func showtoprightmenubtn() {
        if Menu.sharedInstance.isVisible{
            Menu.sharedInstance.handleDismissWithBlackView2()
        }else{
            Menu.sharedInstance.showTopMenu(self)
        }
    }
}
extension UIButton{
    
    func setSelected(){
        setBackgroundImage(UIImage(named: "radio_selected"), for: .normal)
        layer.borderColor = UIColor.clear.cgColor
        layer.borderWidth = 0
        layer.cornerRadius = 0
    }
    func setDeselected(){
        setBackgroundImage(UIImage(named: ""), for: .normal)
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 2.5
        layer.cornerRadius = 15
    }
    func setImage(image: UIImage, titleColor: UIColor, title: String){
        setImage(image, for: .normal)
        titleLabel?.font = .systemFont(ofSize: 14)
        imageView?.contentMode = .scaleAspectFit
        imageView?.tintColor = appcolor
        setTitleColor(titleColor, for: .normal)
        setAttributedTitle(NSAttributedString(string: title), for: .normal)
        titleEdgeInsets = UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 2)
        contentHorizontalAlignment = .left
    }
}
extension UITextView{
    func dropShadow() {
        backgroundColor = UIColor.white
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowRadius = 2
        clipsToBounds = false
    }
}

class Common{
    static var objVC: UIViewController!
}

extension UITextField {
    func setImage(image: UIImage, leftPadding: CGFloat){
//        layer.borderColor = LoginBorderColor.cgColor
//        layer.borderWidth = 1.0
//        backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        layer.cornerRadius = 5
        leftViewMode = .always
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .lightGray//.black
        imageView.frame = CGRect(x: 10, y: 0, width: 20, height: 20)
        let vw = UIView()
        vw.frame = CGRect(x: 20, y: 0, width: 40, height: 20)
        vw.addSubview(imageView)
        leftView = vw
    }
}

func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil && (UserDefaults.standard.object(forKey: key) as? String != "")
}
