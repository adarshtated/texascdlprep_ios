//
//  Common.swift
//  TEXAS CDL PREP
//
//  Created by Sejal on 09/07/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

//let baseUrl =  "https://texascdlanswers.com/temp/api/app/"

let baseUrl = "https://texascdlanswers.com/api/app/"//"https://texascdlanswers.com/temp/api/app/"

public func fetchGenericData<T: Decodable>(urlString: String, parameters: String, method: String = "POST", showLoader: Bool = true, completion: @escaping (T?, String?) -> ()){
    if showLoader{
        LoaderController.sharedInstance.showLoader()
    }
    //urlString == "forget_pass" ? baseUrlFP+urlString :
    let url = URL(string: baseUrl+urlString)
    var request = URLRequest(url: url!)
    print("\n\nurl: \(String(describing: url))\nparameters: \(parameters)")
    request.httpMethod = method//"GET"
    request.httpBody = parameters.data(using: .utf8)
    
    let session = URLSession(configuration: URLSessionConfiguration.default)
    
    session.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            print("error: \(error?.localizedDescription ?? ""))")
            if let x = error as NSError?{
                if x.code == -1009 || x.code == -1005{
                    print("Not connected to internet: \(x.description)")
                    DispatchQueue.main.async{
                        if let topController = getKeywindow()?.rootViewController as? UINavigationController{
                            if let presentedViewController = topController.visibleViewController {
                                presentedViewController.showAlertView(title: "Alert", message: "Network error. Please try again.")
                            }
                        }
                    }
                }
            }
            LoaderController.sharedInstance.removeLoader()
            return
        }
        let str = String(decoding: data, as: UTF8.self)
        print("fetched data: \(str)")
        
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("------statusCode should be 200, but is: \(httpStatus.statusCode)\n response \(String(describing: response))")
            if showLoader{
                LoaderController.sharedInstance.removeLoader()
            }
            DispatchQueue.main.async{
                LoaderController.sharedInstance.removeLoader()
                if let topController = getKeywindow()?.rootViewController as? UINavigationController{
                    if let presentedViewController = topController.visibleViewController {
                        presentedViewController.showAlertView(title: "Alert", message: "Server Error.")
                    }
                }
            }
            return
        }
        do{
            let obj = try JSONDecoder().decode(T.self, from: data)
            print("completed with success")
            completion(obj, nil)
        }catch let jsonErr{
            completion(nil, str)
            print("failed to convert json: \(jsonErr.localizedDescription) , \n jsonErr : ",jsonErr)
        }
        
        if showLoader{
            LoaderController.sharedInstance.removeLoader()
        }
    }.resume()
}

func getKeywindow() -> UIWindow?{
    UIApplication.shared.windows.filter {$0.isKeyWindow}.first ?? nil
}
func getUserId() -> String{
    UserDefaults.standard.value(forKey: "userId") as? String ?? ""
}
extension UIViewController{
    
    func showAlertView(title:String,message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor?, paddingTop: CGFloat, bottom: NSLayoutYAxisAnchor?, paddingBottom: CGFloat, left: NSLayoutXAxisAnchor?, paddingLeft: CGFloat, right: NSLayoutXAxisAnchor?, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}

class CommonTextfield: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()

    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        leftViewMode = .always
        let vw = UIView()
        vw.frame = CGRect(x: 20, y: 0, width: 5, height: 20)
        leftView = vw
        self.layer.cornerRadius = 5.0
        layer.borderWidth = 1
        layer.borderColor = UIColor.init(white: 0.8, alpha: 1).cgColor
    }
}
class Login: Codable {
    var status: Bool
    var message: String
    var result: [LoginResult]?
}
class LoginResult: Codable{
    var ID: String
    var user_login: String
    var user_nicename: String
    var user_email: String
    var user_registered: String
    var user_status: String
}
class Reg: Codable {
    var status: Bool
    var message: String
    var result: [RegResult]?
}
class RegResult: Codable{
    var user_id: String
    var username: String
    var email: String
}
let appcolor = UIColor.init(red: 9/255, green: 61/255, blue: 118/255, alpha: 1)

class AllOrdersLogin: Codable {
    var status: Bool
    var message: String
    var result: [AllOrders]?
}
class AllOrders: Codable{
    var product_name: String
    var product_price: String
    var expire_date: String?
}
