//
//  Menu.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.


import UIKit
import MessageUI
import Social
import Cosmos

let storyboard = UIStoryboard(name: "Main", bundle: nil)
var nav = NavViewController()

class Menu: NSObject {

    static let sharedInstance = Menu()
    
    let topview = UIStackView()
    var viewc: UIViewController!
    var btn_about = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    var btn_exit = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    var btn_lang = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    var btn_login = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    var btn_reg = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    let blackview2 = UIView()
    var isVisible = false

    override init() {
        btn_about.setTitleColor(UIColor.black, for: .normal)
        btn_exit.setTitleColor(UIColor.black, for: .normal)
        btn_lang.setTitleColor(UIColor.black, for: .normal)
        btn_login.setTitleColor(UIColor.black, for: .normal)
        btn_reg.setTitleColor(UIColor.black, for: .normal)
        btn_about.setTitle("aboutus".localized(), for: .normal)
        btn_exit.setTitle("EXIT", for: .normal)
        btn_lang.setTitle("English | Spanish", for: .normal)
        
        
        topview.addArrangedSubview(btn_about)
        topview.addArrangedSubview(btn_exit)
        topview.addArrangedSubview(btn_lang)
        
        topview.axis = NSLayoutConstraint.Axis.vertical
        topview.distribution = UIStackView.Distribution.equalSpacing
        topview.alignment = UIStackView.Alignment.fill
        
        if isKeyPresentInUserDefaults(key: "userId"){
            btn_login.setTitle("SYNC", for: .normal)
            btn_login.tag = 10 // for sync
            topview.addArrangedSubview(btn_login)
        }else{
            btn_login.setTitle("LOGIN", for: .normal)
            btn_reg.setTitle("REGISTER", for: .normal)
            topview.addArrangedSubview(btn_login)
            topview.addArrangedSubview(btn_reg)
        }
        topview.backgroundColor = UIColor.white
    }


    @objc func showTopMenu(_ objVC : UIViewController!){
//        print("showTopMenu")
        if isKeyPresentInUserDefaults(key: "userId"){
            topview.frame = CGRect(x: UIScreen.main.bounds.width - 200, y: 64, width: 200, height: 200)
            btn_login.setTitle("SYNC", for: .normal)
            btn_login.tag = 10 // for sync
            btn_reg.removeFromSuperview()
        }
        self.viewc = objVC
        if let window = UIApplication.shared.keyWindow{
            blackview2.frame = window.frame
            blackview2.alpha = 1
            blackview2.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackview2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissWithBlackView2)))
            btn_about.addTarget(self, action: #selector(aboutUs), for: .touchUpInside)
            btn_exit.addTarget(self, action: #selector(exitApplication), for: .touchUpInside)
            btn_lang.addTarget(self, action: #selector(changeLanguage), for: .touchUpInside)
            btn_login.addTarget(self, action: #selector(showLogin), for: .touchUpInside)
            btn_reg.addTarget(self, action: #selector(showRegister), for: .touchUpInside)
            blackview2.addSubview(topview)
            
            topview.translatesAutoresizingMaskIntoConstraints = false
            topview.topAnchor.constraint(equalTo: blackview2.safeAreaLayoutGuide.topAnchor).isActive = true
            topview.widthAnchor.constraint(equalToConstant: 200).isActive = true
            topview.heightAnchor.constraint(equalToConstant: 200).isActive = true
            topview.rightAnchor.constraint(equalTo: blackview2.rightAnchor).isActive = true
            
            viewc.view.addSubview(blackview2)
            isVisible = true
        }
    }

    @objc func exitApplication(){
        handleDismissWithBlackView2()
        exit(0)
    }
    
    @objc func showLogin(_ sender: UIButton){
        handleDismissWithBlackView2()
        if sender.tag == 10{ // sync
            let _ = SyncView(frame: self.viewc.view.frame, viewController: self.viewc, topView: self.viewc.view)
        }else{
            let next = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let obj = Common.objVC as! StartPageViewController
            obj.navigationController?.pushViewController(next, animated: true)
//            let _ = LoginView(frame: self.viewc.view.frame, viewController: self.viewc, topView: self.viewc.view)
        }
    }
    
    @objc func showRegister(){
        handleDismissWithBlackView2()
        let next = storyboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        let obj = Common.objVC as! StartPageViewController
        obj.navigationController?.pushViewController(next, animated: true)
//        let _ = RegisterView(frame: self.viewc.view.frame, viewController: self.viewc, topView: self.viewc.view)
    }

    @objc func changeLanguage(){
        handleDismissWithBlackView2()
        UserDefaults.standard.set(!(UserDefaults.standard.value(forKey: "isEnglish") as? Bool ?? true), forKey: "isEnglish")
        UserDefaults.standard.synchronize()
        print("isEnglish: \(UserDefaults.standard.value(forKey: "isEnglish") as! Bool)")
        if Common.objVC.isKind(of: StartPageViewController.self){
            let obj = Common.objVC as! StartPageViewController
            obj.changeLanguage()
        }else if Common.objVC.isKind(of: SelectQuizViewController.self){
            let obj = Common.objVC as! SelectQuizViewController
            obj.changeLanguage()
        }else if Common.objVC.isKind(of: StartQuizViewController.self){
            let obj = Common.objVC as! StartQuizViewController
            obj.changeLanguage()
        }
    }

    @objc func aboutUs(){
        if let url = URL(string: "https://www.texascdlprep.com") {
            UIApplication.shared.open(url, options: [:])
        }
        handleDismissWithBlackView2()
    }


    @objc func handleDismissWithBlackView2(){
        //top right menu
        print("removing top right")
        topview.removeFromSuperview()
        blackview2.removeFromSuperview()
        isVisible = false
    }


}
