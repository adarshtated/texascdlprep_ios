//
//  LoginView.swift
//  TEXAS CDL PREP
//
//  Created by Sejal on 08/07/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit


class RegisterView: UIView {
    
    let imgView = UIImageView()
    let txtName = CommonTextfield()
    let txtEmail = CommonTextfield()
    let txtPassword = CommonTextfield()
    let btnRegister = UIButton(type: .system)
//    let btnCross = UIButton(type: .system)
    let blackview = UIView()
    var topview: UIView!
    var vc: UIViewController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(frame: CGRect, viewController: UIViewController, topView: UIView) {
        self.init(frame: frame)
        topview = topView
        vc = viewController
        imgView.image = UIImage(named: "4096x4096.png")
        txtName.placeholder = "Enter User Name"
        if #available(iOS 13.0, *) {
            txtName.setImage(image: UIImage(systemName: "person.crop.rectangle.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        txtEmail.placeholder = "Enter Email"
        txtEmail.keyboardType = .emailAddress
        if #available(iOS 13.0, *) {
            txtEmail.setImage(image: UIImage(systemName: "envelope.fill")!, leftPadding: 20)//person.fill
        } else {
            // Fallback on earlier versions
        }
        txtPassword.placeholder = "Enter Password"
        txtPassword.isSecureTextEntry = true
        if #available(iOS 13.0, *) {
            txtPassword.setImage(image: UIImage(systemName: "lock.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        btnRegister.backgroundColor = appcolor
        btnRegister.setTitle("REGISTER", for: .normal)
        btnRegister.tintColor = .white
        btnRegister.layer.cornerRadius = 20
//        btnCross.setImage(UIImage(named: "close.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        blackview.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
        setUpView(viewController: viewController, topView: topView)
    }
    
    func setUpView(viewController: UIViewController, topView: UIView){
        
        let width = CGFloat(screenWidth * 0.9)
        let textHeight = CGFloat(40)
        let topSpace = CGFloat(15)
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 5
        
        topView.addSubview(blackview)
        blackview.anchor(top: topView.topAnchor, paddingTop: 0, bottom: topView.bottomAnchor, paddingBottom: 0, left: topView.leftAnchor, paddingLeft: 0, right: topView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        blackview.addSubview(self)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.centerXAnchor.constraint(equalTo: blackview.centerXAnchor, constant: 0).isActive = true
        self.centerYAnchor.constraint(equalTo: blackview.centerYAnchor, constant: 0).isActive = true
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.backgroundColor = .white
        
//        self.addSubview(btnCross)
//        btnCross.anchor(top: self.topAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: 50, height: 50)
//        btnCross.addTarget(self, action: #selector(removeDynamicView), for: .touchUpInside)
        
        self.addSubview(imgView)
        imgView.anchor(top: self.topAnchor, paddingTop: topSpace * 2, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: screenWidth * 0.5, height: screenWidth * 0.4)
        imgView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(txtName)
        txtName.translatesAutoresizingMaskIntoConstraints = false
        txtName.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: topSpace).isActive = true
        txtName.heightAnchor.constraint(equalToConstant: textHeight).isActive = true
        txtName.widthAnchor.constraint(equalToConstant: width * 0.8).isActive = true
        txtName.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(txtEmail)
        txtEmail.translatesAutoresizingMaskIntoConstraints = false
        txtEmail.topAnchor.constraint(equalTo: txtName.bottomAnchor, constant: topSpace).isActive = true
        txtEmail.heightAnchor.constraint(equalToConstant: textHeight).isActive = true
        txtEmail.widthAnchor.constraint(equalToConstant: width * 0.8).isActive = true
        txtEmail.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(txtPassword)
        txtPassword.translatesAutoresizingMaskIntoConstraints = false
        txtPassword.topAnchor.constraint(equalTo: txtEmail.bottomAnchor, constant: topSpace).isActive = true
        txtPassword.heightAnchor.constraint(equalToConstant: textHeight).isActive = true
        txtPassword.widthAnchor.constraint(equalToConstant: width * 0.8).isActive = true
        txtPassword.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(btnRegister)
        btnRegister.translatesAutoresizingMaskIntoConstraints = false
        btnRegister.topAnchor.constraint(equalTo: txtPassword.bottomAnchor, constant: topSpace).isActive = true
        btnRegister.heightAnchor.constraint(equalToConstant: 40).isActive = true
        btnRegister.widthAnchor.constraint(equalToConstant: width * 0.6).isActive = true
        btnRegister.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        btnRegister.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -topSpace).isActive = true
        
        btnRegister.addTarget(self, action: #selector(callRegApi), for: .touchUpInside)
    }
    
    @objc func removeDynamicView(){
        self.removeFromSuperview()
        self.blackview.removeFromSuperview()
    }
    
    @objc func callRegApi(){
        if txtEmail.text == "" || txtPassword.text == "" || txtName.text == ""{
            vc.showAlertView(title: "Alert", message: "Cannot leave any field empty.")
            return;
        }
        if txtName.text!.contains(" "){
            vc.showAlertView(title: "Alert", message: "Username should not contain space")
            return;
        }
        fetchGenericData(urlString: "registration", parameters: "username=\(txtName.text!)&email=\(txtEmail.text!)&password=\(txtPassword.text!)", completion: {(objCommonResponse: Reg?, str) in
            DispatchQueue.main.async {
                if let objCommonResponse = objCommonResponse{
                    if objCommonResponse.status {
                        self.removeDynamicView()
                        self.vc.showAlertView(title: "Alert", message: objCommonResponse.message)
                        UserDefaults.standard.setValue(objCommonResponse.result![0].user_id, forKey: "userId")
                        UserDefaults.standard.synchronize()
                        self.vc?.getAllOrder(showAlert: true)
                    }else{
                        self.vc.showAlertView(title: "Alert", message: objCommonResponse.message)
                        LoaderController.sharedInstance.removeLoader()
                    }
                }else{
                    LoaderController.sharedInstance.removeLoader()
                }
            }
        })
    }
    deinit {
        print("send csv deinit")
    }
}
