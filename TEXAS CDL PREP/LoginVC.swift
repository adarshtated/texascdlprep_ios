//
//  LoginVC.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 26/04/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var txtEmail: CommonTextfield!
    @IBOutlet weak var txtPassword: CommonTextfield!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Login"
        if #available(iOS 13.0, *) {
            txtEmail.setImage(image: UIImage(systemName: "envelope.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 13.0, *) {
            txtPassword.setImage(image: UIImage(systemName: "lock.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        
        btnLogin.backgroundColor = appcolor
        btnLogin.layer.cornerRadius = 20
        setUpMenuButton()
        setUpRightMenuButton()

    }
    
    @IBAction func onClickLogin(_ sender: Any) {
        callLoginApi()
    }
    
    @IBAction func onClickForgotPassword(_ sender: Any) {
        callForgotPassword()
    }
    @IBAction func onClickRegistration(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func callLoginApi(){
        if txtEmail.text == "" || txtPassword.text == ""{
            showAlertView(title: "Alert", message: "Cannot leave any field empty.")
            return;
        }
        fetchGenericData(urlString: "user_login", parameters: "username=\(txtEmail.text!)&password=\(txtPassword.text!)", completion: {(objCommonResponse: Login?, str) in
            DispatchQueue.main.async {
                if let objCommonResponse = objCommonResponse{
                    if objCommonResponse.status {
//                        let _ = SyncView(frame: self.vc.view.frame, viewController: self.vc, topView: self.vc.view)
                        IAPHandler.shared.tryCheckValidateReceiptAndUpdateExpirationDate()
                        UserDefaults.standard.setValue(objCommonResponse.result![0].ID, forKey: "userId")
                        UserDefaults.standard.synchronize()
                        let next = self.storyboard?.instantiateViewController(withIdentifier: "StartPageViewController") as! StartPageViewController
                        self.navigationController?.pushViewController(next, animated: true)
                        
                        self.getAllOrder(showAlert: true)
                        
                    }else{
                        self.showAlertView(title: "Alert", message: objCommonResponse.message)
                    }
                }
                LoaderController.sharedInstance.removeLoader()
            }
        })
    }
    
    func callForgotPassword(){
        let _ = ForgotPassView(frame: self.view.frame, viewController: self, topView: self.view)
    }
    
}


