//
//  LoginView.swift
//  TEXAS CDL PREP
//
//  Created by Sejal on 08/07/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height

class LoginView: UIView {
    
    let imgView = UIImageView()
    let txtEmail = CommonTextfield()
    let txtPassword = CommonTextfield()
    let btnLogin = UIButton(type: .system)
    let btnForgotPassword = UIButton(type: .system)
//    let btnCross = UIButton(type: .system)
    let blackview = UIView()
    var topview: UIView!
    var vc: UIViewController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(frame: CGRect, viewController: UIViewController, topView: UIView) {
        self.init(frame: frame)
        topview = topView
        vc = viewController
        imgView.image = UIImage(named: "4096x4096.png")
        txtEmail.placeholder = "Enter Email / User Name"
        if #available(iOS 13.0, *) {
            txtEmail.setImage(image: UIImage(systemName: "envelope.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        txtPassword.placeholder = "Enter Password"
        txtPassword.isSecureTextEntry = true
        if #available(iOS 13.0, *) {
            txtPassword.setImage(image: UIImage(systemName: "lock.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        
        btnLogin.backgroundColor = appcolor
        btnLogin.setTitle("LOGIN", for: .normal)
        btnLogin.tintColor = .white
        btnLogin.layer.cornerRadius = 20
        
        btnForgotPassword.setTitle("Forgot Password?", for: .normal)
        btnForgotPassword.tintColor = .red
        
//        btnCross.setImage(UIImage(named: "close.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        blackview.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
        setUpView(viewController: viewController, topView: topView)
    }
    
    func setUpView(viewController: UIViewController, topView: UIView){
        
        let width = CGFloat(screenWidth * 0.9)
        let textHeight = CGFloat(35)
        let topSpace = CGFloat(15)
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 5
        
        topView.addSubview(blackview)
        blackview.anchor(top: topView.topAnchor, paddingTop: 0, bottom: topView.bottomAnchor, paddingBottom: 0, left: topView.leftAnchor, paddingLeft: 0, right: topView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        
        blackview.addSubview(self)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.centerXAnchor.constraint(equalTo: blackview.centerXAnchor, constant: 0).isActive = true
        self.centerYAnchor.constraint(equalTo: blackview.centerYAnchor, constant: 0).isActive = true
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.backgroundColor = .white
        
//        self.addSubview(btnCross)
//        btnCross.anchor(top: self.topAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: 50, height: 50)
//        btnCross.addTarget(self, action: #selector(removeDynamicView), for: .touchUpInside)
        
        self.addSubview(imgView)
        imgView.anchor(top: self.topAnchor, paddingTop: topSpace * 2, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: screenWidth * 0.5, height: screenWidth * 0.4)
        imgView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(txtEmail)
        txtEmail.translatesAutoresizingMaskIntoConstraints = false
        txtEmail.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: topSpace).isActive = true
        txtEmail.heightAnchor.constraint(equalToConstant: textHeight).isActive = true
        txtEmail.widthAnchor.constraint(equalToConstant: width * 0.8).isActive = true
        txtEmail.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        txtEmail.keyboardType = .emailAddress
        
        self.addSubview(txtPassword)
        txtPassword.translatesAutoresizingMaskIntoConstraints = false
        txtPassword.topAnchor.constraint(equalTo: txtEmail.bottomAnchor, constant: topSpace).isActive = true
        txtPassword.heightAnchor.constraint(equalToConstant: textHeight).isActive = true
        txtPassword.widthAnchor.constraint(equalToConstant: width * 0.8).isActive = true
        txtPassword.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(btnLogin)
        btnLogin.translatesAutoresizingMaskIntoConstraints = false
        btnLogin.topAnchor.constraint(equalTo: txtPassword.bottomAnchor, constant: topSpace).isActive = true
        btnLogin.heightAnchor.constraint(equalToConstant: 40).isActive = true
        btnLogin.widthAnchor.constraint(equalToConstant: width * 0.6).isActive = true
        btnLogin.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        //btnLogin.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -topSpace).isActive = true
        btnLogin.addTarget(self, action: #selector(callLoginApi), for: .touchUpInside)
        
        self.addSubview(btnForgotPassword)
        btnForgotPassword.translatesAutoresizingMaskIntoConstraints = false
        btnForgotPassword.topAnchor.constraint(equalTo: btnLogin.bottomAnchor, constant: 10).isActive = true
        btnForgotPassword.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btnForgotPassword.widthAnchor.constraint(equalToConstant: width * 0.6).isActive = true
        btnForgotPassword.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 5).isActive = true
        btnForgotPassword.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -topSpace).isActive = true
        btnForgotPassword.addTarget(self, action: #selector(callForgotPassword), for: .touchUpInside)
        
        
    }
    
    @objc func removeDynamicView(){
        self.removeFromSuperview()
        self.blackview.removeFromSuperview()
    }
    
    @objc func callLoginApi(){
        if txtEmail.text == "" || txtPassword.text == ""{
            vc.showAlertView(title: "Alert", message: "Cannot leave any field empty.")
            return;
        }
        fetchGenericData(urlString: "user_login", parameters: "username=\(txtEmail.text!)&password=\(txtPassword.text!)", completion: {(objCommonResponse: Login?, str) in
            DispatchQueue.main.async {
                if let objCommonResponse = objCommonResponse{
                    if objCommonResponse.status {
//                        let _ = SyncView(frame: self.vc.view.frame, viewController: self.vc, topView: self.vc.view)
                        UserDefaults.standard.setValue(objCommonResponse.result![0].ID, forKey: "userId")
                        UserDefaults.standard.synchronize()
                        self.vc?.getAllOrder(showAlert: true)
                        self.removeDynamicView()
                    }else{
                        self.vc.showAlertView(title: "Alert", message: objCommonResponse.message)
                    }
                }
                LoaderController.sharedInstance.removeLoader()
            }
        })
    }
    
    @objc func callForgotPassword(){
        let _ = ForgotPassView(frame: self.frame, viewController: self.vc, topView: self)
    }
    
    deinit {
        print("send csv deinit")
    }
}
