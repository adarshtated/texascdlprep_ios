//
//  SelectQuizViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import StoreKit

var selectProductIndex = Int()

enum IAPHandlerAlertType{
    case disabled
    case restored
    case purchased
    
    func message() -> String{
        switch self {
        case .disabled: return "Purchases are disabled in your device!"
        case .restored: return "You've successfully restored your purchase!"
        case .purchased: return "You've successfully bought this purchase!"
        }
    }
}


class IAPHandler: NSObject {
    static let shared = IAPHandler()
    
    fileprivate var productID = ""
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var iapProducts = [SKProduct]()
    
    var purchaseStatusBlock: ((IAPHandlerAlertType) -> Void)?
    
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool { print("SKPaymentQueue.canMakePayments- ",SKPaymentQueue.canMakePayments())
        return SKPaymentQueue.canMakePayments()  }
    
    func purchaseMyProduct(index: Int){
        if iapProducts.count == 0 {
            print("no schemes available")
            let alertView = UIAlertController(title: "", message: "No Scheme Available", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            })
            alertView.addAction(action)
            selectQuizViewController.present(alertView, animated: true, completion: nil)
            return }
        if self.canMakePurchases() {
            let product = iapProducts[index]
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
            productID = product.productIdentifier
        } else {
            print("")
            let alertController = UIAlertController(title: "Alert", message: "Not Available", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            purchaseStatusBlock?(.disabled)
            return
        }
    }
    
    // MARK: - RESTORE PURCHASE
    func restorePurchase(){
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    
    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts(){
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects:         "Full_key_package_sb_1","Permit_pass_package_sb_1","Combination_key_package_sb_1","General_knowledge_key_package_sb_1","Pretrip_inspection_sb_1","Commercial_vehicle_sb_1","Airbrakes_endorsement_Sb_1","Hazmat_endorsement_sb_1","Tanker_endorsement_sb_1","Passenger_bus_endorsement_sb_1","Doubles_triples_endorsement_sb_1","School_bus_endorsement_sb_1")//SelectQuizViewController.chosen_package)
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    // MARK: - FETCH RECEIPT FOR EXPIRAY DATE
    
    // SandBox Id:- osiyatest@gmail.com
    // Pass :- Q1*q1*q1*
    func tryCheckValidateReceiptAndUpdateExpirationDate() {
        var _: [Int] = []
        var productTag = ""
        var expiryDate = ""
        var auto_renew_product_id = ""
        var auto_renew_status = ""
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
           FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            print("^A receipt found. Validating it...")
            // We will allow user to use all premium features until
            //DispatchQueue.main.async {
                
            //}
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                let receiptString = receiptData.base64EncodedString(options: [])
                let dict = ["receipt-data" : receiptString, "password" : "accdf0c8089e42868ffb5b5eb3eeb5fd"] as [String : Any]
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                    if let storeURL = Foundation.URL(string:"https://buy.itunes.apple.com/verifyReceipt"),
                       let sandboxURL = Foundation.URL(string: "https://sandbox.itunes.apple.com/verifyReceipt") {
                        var request = URLRequest(url: storeURL)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        let session = URLSession(configuration: URLSessionConfiguration.default)
                        NSLog("^Connecting to production...")
                        LoaderController.sharedInstance.showLoader()
                        let task = session.dataTask(with: request) { data, response, error in
                            if let receivedData = data, let httpResponse = response as? HTTPURLResponse,
                               error == nil, httpResponse.statusCode == 200 {
                                print("^Received 200, verifying data...")
                                
                                do {
                                    if let jsonResponse = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>,
                                       let status = jsonResponse["status"] as? Int64 {
                                        print("jsonResponse~~~~~",jsonResponse)
                                        
                                        switch status {
                                        case 0:
                                            
                                            if let pending_renewal_info: NSArray = jsonResponse["pending_renewal_info"] as? NSArray{
                                                let lastReceiptInfo = pending_renewal_info.firstObject as? NSDictionary
                                                print("anushka 0---0 \(String(describing: pending_renewal_info))")
                                                auto_renew_product_id = lastReceiptInfo?["auto_renew_product_id"] as? String ?? ""
                                                auto_renew_status = lastReceiptInfo?["auto_renew_status"] as? String ?? ""
                                                if let auto_renew_status = lastReceiptInfo?["auto_renew_status"] as? String{
                                                    UserDefaults.standard.set(auto_renew_status, forKey: "auto_renew_status")
                                                    if let expiration_intent = lastReceiptInfo?["expiration_intent"] as? String{
                                                        UserDefaults.standard.set(expiration_intent, forKey: "expiration_intent")
                                                    }
                                                }
                                                print("anushka --- \(String(describing: lastReceiptInfo))")
                                            }
                                            
                                            if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray{
                                                let formatter = DateFormatter()
                                                formatter.dateFormat = "yyyy-MM-dd"
                                                for i in 0..<receiptInfo.count{
                                                    let dic = receiptInfo[i] as? NSDictionary
                                                    let produtId = dic?.object(forKey: "product_id") as? String ?? ""
                                                    if produtId == "Full_key_package_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("full-key"){
                                                            productTag += "full-key,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "FKPExpires_date")
                                                        }
                                                        
                                                    }else if produtId == "Permit_pass_package_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("permit-pass"){
                                                            productTag += "permit-pass,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "PPPExpires_date")
                                                        }
                                                    }else if produtId == "Combination_key_package_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("combination"){
                                                            productTag += "combination,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "CKPExpires_date")
                                                        }
                                                    }else if produtId == "General_knowledge_key_package_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        
                                                        expires_date.removeLast(17)
                                                        
                                                        if !productTag.contains("general-knowledge"){
                                                            productTag += "general-knowledge,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "GKExpires_date")
                                                        }
                                                        print("GKExpires_date",expires_date)
                                                        
                                                    }else if produtId == "Pretrip_inspection_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("pre-trip"){
                                                            productTag += "pre-trip,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "PIExpires_date")
                                                        }
                                                    }else if produtId == "Commercial_vehicle_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("commercial-vehicle"){
                                                            productTag += "commercial-vehicle,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "CVExpires_date")
                                                        }
                                                    }else if produtId == "Airbrakes_endorsement_Sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("air-brake"){
                                                            productTag += "air-brake,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "AEExpires_date")
                                                        }
                                                    }else if produtId == "Hazmat_endorsement_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        
                                                        if !productTag.contains("hazmat"){
                                                            productTag += "hazmat,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "HEExpires_date")
                                                        }
                                                    }else if produtId == "Tanker_endorsement_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("tanker"){
                                                            productTag += "tanker,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "TEExpires_date")
                                                        }
                                                    }else if produtId == "Passenger_bus_endorsement_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("passenger"){
                                                            productTag += "passenger,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "PBExpires_date")
                                                        }
                                                    }else if produtId == "Doubles_triples_endorsement_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("doubles-triples"){
                                                            productTag += "doubles-triples,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "DTExpires_date")
                                                        }
                                                    }else if produtId == "School_bus_endorsement_sb_1"{
                                                        var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                        expires_date.removeLast(17)
                                                        if !productTag.contains("school-bus"){
                                                            productTag += "school-bus,"
                                                            expiryDate += "\(expires_date),"
                                                            UserDefaults.standard.set("\(expires_date)", forKey: "SBExpires_date")
                                                        }
                                                    }
                                                }
                                                
                                                DispatchQueue.main.async {
                                                    //                                                    if productTag != "" && expiryDate != ""{
                                                    //                                                        selectQuizViewController.updateExpireDate(productTag: "\(productTag.removeLast())", productExpiry: "\(expiryDate.removeLast())")
                                                    //                                                    }
                                                }
                                                if let lastReceipt = receiptInfo.lastObject as? NSDictionary{
                                                    UserDefaults.standard.set(lastReceipt["purchase_date_ms"], forKey: "datedata")
                                                    UserDefaults.standard.set(lastReceipt["expires_date"], forKey: "expirydatedata")
                                                    UserDefaults.standard.set(lastReceipt["is_trial_period"], forKey: "is_trial_period")
                                                    print("anushka: \(lastReceipt)")
                                                }
                                            }
                                            UserDefaults.standard.setValue("", forKey: "InAppRefresh")
                                            
                                            if isDataGetEmpty == true{
                                                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                                            }
                                            
                                            
                                            NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
                                            
                                        //                                            DispatchQueue.main.async {
                                        //                                                if controllerobj.strRestore == "strRestore"{
                                        //                                                    controllerobj.strRestore = ""
                                        //                                                    controllerobj.alartview(str: "Restore successfully")
                                        //                                                }
                                        //                                                controllerobj.restoreBtn.isHidden = true
                                        //                                            }
                                        case 21007:
                                            print("^need to repeat evrything with Sandbox")
                                            var request = URLRequest(url: sandboxURL)
                                            request.httpMethod = "POST"
                                            request.httpBody = jsonData
                                            let session = URLSession(configuration: URLSessionConfiguration.default)
                                            print("^Connecting to Sandbox...")
                                            let task = session.dataTask(with: request) { data, response, error in
                                                if let receivedData = data, let httpResponse = response as? HTTPURLResponse,
                                                   error == nil, httpResponse.statusCode == 200 {
                                                    print("^Received 200, verifying data...")
                                                    
                                                    do {
                                                        if let jsonResponse = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>,
                                                           let status = jsonResponse["status"] as? Int64 {
                                                            print("jsonResponse~~~123~~",jsonResponse)
                                                            
                                                            switch status {
                                                            case 0:
                                                                
                                                                DispatchQueue.main.async {
                                                                    LoaderController.sharedInstance.removeLoader()
                                                                }
                                                                if let pending_renewal_info: NSArray = jsonResponse["pending_renewal_info"] as? NSArray{
                                                                    let lastReceiptInfo = pending_renewal_info.firstObject as? NSDictionary
                                                                    print("anushka 0---0 \(String(describing: pending_renewal_info))")
                                                                    auto_renew_product_id = lastReceiptInfo?["auto_renew_product_id"] as? String ?? ""
                                                                    auto_renew_status = lastReceiptInfo?["auto_renew_status"] as? String ?? ""
                                                                    print("auto_renew_status",auto_renew_status)
                                                                    if let auto_renew_status = lastReceiptInfo?["auto_renew_status"] as? String{
                                                                        UserDefaults.standard.set(auto_renew_status, forKey: "auto_renew_status")
                                                                        if let expiration_intent = lastReceiptInfo?["expiration_intent"] as? String{
                                                                            UserDefaults.standard.set(expiration_intent, forKey: "expiration_intent")
                                                                        }
                                                                    }
                                                                    print("anushka --- \(String(describing: lastReceiptInfo))")
                                                                }
                                                                
                                                                if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray{
                                                                    let formatter = DateFormatter()
                                                                    formatter.dateFormat = "yyyy-MM-dd"
                                                                    for i in 0..<receiptInfo.count{
                                                                        let dic = receiptInfo[i] as? NSDictionary
                                                                        let produtId = dic?.object(forKey: "product_id") as? String ?? ""
                                                                        if produtId == "Full_key_package_sb_1"{
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("full-key"){
                                                                                productTag += "full-key,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "FKPExpires_date")
                                                                            }
                                                                            
                                                                        }else if produtId == "Permit_pass_package_sb_1"{
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("permit-pass"){
                                                                                productTag += "permit-pass,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "PPPExpires_date")
                                                                            }
                                                                        }else if produtId == "Combination_key_package_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("combination"){
                                                                                productTag += "combination,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "CKPExpires_date")
                                                                            }
                                                                        }else if produtId == "General_knowledge_key_package_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            
                                                                            expires_date.removeLast(17)
                                                                            
                                                                            if !productTag.contains("general-knowledge"){
                                                                                productTag += "general-knowledge,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "GKExpires_date")
                                                                            }
                                                                            print("GKExpires_date",expires_date)
                                                                            
                                                                        }else if produtId == "Pretrip_inspection_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("pre-trip"){
                                                                                productTag += "pre-trip,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "PIExpires_date")
                                                                            }
                                                                        }else if produtId == "Commercial_vehicle_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("commercial-vehicle"){
                                                                                productTag += "commercial-vehicle,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "CVExpires_date")
                                                                            }
                                                                        }else if produtId == "Airbrakes_endorsement_Sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("air-brake"){
                                                                                productTag += "air-brake,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "AEExpires_date")
                                                                            }
                                                                        }else if produtId == "Hazmat_endorsement_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            
                                                                            if !productTag.contains("hazmat"){
                                                                                productTag += "hazmat,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "HEExpires_date")
                                                                            }
                                                                        }else if produtId == "Tanker_endorsement_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("tanker"){
                                                                                productTag += "tanker,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "TEExpires_date")
                                                                            }
                                                                        }else if produtId == "Passenger_bus_endorsement_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("passenger"){
                                                                                productTag += "passenger,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "PBExpires_date")
                                                                            }
                                                                        }else if produtId == "Doubles_triples_endorsement_sb_1"{
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("doubles-triples"){
                                                                                productTag += "doubles-triples,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "DTExpires_date")
                                                                            }
                                                                        }else if produtId == "School_bus_endorsement_sb_1" {
                                                                            var expires_date = dic?.object(forKey: "expires_date") as? String ?? ""
                                                                            expires_date.removeLast(17)
                                                                            if !productTag.contains("school-bus"){
                                                                                productTag += "school-bus,"
                                                                                expiryDate += "\(expires_date),"
                                                                                UserDefaults.standard.set("\(expires_date)", forKey: "SBExpires_date")
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    DispatchQueue.main.async {
                                                                        //selectQuizViewController.updateExpireDate(productTag: "\(productTag.dropLast())", productExpiry: "\(expiryDate.dropLast())")
                                                                    }
                                                                    
                                                                    
                                                                    if let lastReceipt = receiptInfo.firstObject as? NSDictionary{
                                                                        UserDefaults.standard.set(lastReceipt["purchase_date_ms"], forKey: "datedata")
                                                                        UserDefaults.standard.set(lastReceipt["expires_date"], forKey: "expirydatedata")
                                                                        UserDefaults.standard.set(lastReceipt["is_trial_period"], forKey: "is_trial_period")
                                                                        print("anushka: \(lastReceipt)")
                                                                    }
                                                                }
                                                                UserDefaults.standard.setValue("", forKey: "InAppRefresh")
                                                                NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
                                                                if isDataGetEmpty == true{
                                                                    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                                                                }
                                                                NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
                                                                
                                                            //                                                                DispatchQueue.main.async {
                                                            //                                                                    if controllerobj.strRestore == "strRestore"{
                                                            //                                                                        controllerobj.strRestore = ""
                                                            //                                                                        controllerobj.alartview(str: "Restore successfully")
                                                            //                                                                    }
                                                            //                                                                    controllerobj.restoreBtn.isHidden = true
                                                            //selectQuizViewController.showAlertView(title: "", message: "Restore successfully")
                                                            //                                                                }
                                                            //
                                                            default:
                                                                print("^ succesfull---")
                                                            }
                                                        } else {
                                                            DispatchQueue.main.async {
                                                                selectQuizViewController.showAlertView(title: "", message: "Failed to cast serialized JSON to Dictionary<String, AnyObject>")
                                                            }
                                                            
                                                        }
                                                    }
                                                    catch {
                                                        DispatchQueue.main.async {selectQuizViewController.showAlertView(title: "", message: "Couldn't serialize JSON with error: " + error.localizedDescription)}
                                                        
                                                    }
                                                } else { //self.handleNetworkError(data: data, response: response, error: error)
                                                    
                                                }
                                            }
                                            // END of closure #2 = verification with Sandbox
                                            task.resume()
                                            DispatchQueue.main.async {
                                                //selectQuizViewController.showAlertView(title: "", message: "\(status)")
                                                
                                            }
                                        //                                        default: self.showAlertWithErrorCode(errorCode: status)
                                        case 21004:
                                            DispatchQueue.main.async {
                                                //saveException("21004: The shared secret you provided does not match the shared secret on file for your account.")
                                            }
                                        case 21006: //This receipt is valid but the subscription has expired
                                            DispatchQueue.main.async {
                                                //homeVCobj.activityIndicator.stopAnimating()
                                                selectQuizViewController.showAlertView(title: "", message: "Your receipt has been cancelled. Please purchase again.")
                                                UserDefaults.standard.set(true, forKey: "BoolNotPurchased")
                                            }
                                        case 21009:
                                            DispatchQueue.main.async {
                                                //homeVCobj.activityIndicator.stopAnimating()
                                                selectQuizViewController.showAlertView(title: "", message: "Internal data access error. Try again later.")
                                            }
                                        case 21010:
                                            DispatchQueue.main.async {
                                                //homeVCobj.activityIndicator.stopAnimating()
                                                selectQuizViewController.showAlertView(title: "", message: "The user account cannot be found or has been deleted.")
                                            }
                                        default:
                                            print("default0123")
                                        }
                                    } else {  DispatchQueue.main.async {selectQuizViewController.showAlertView(title: "", message: "Failed to cast serialized JSON to Dictionary<String, AnyObject>")}
                                        
                                    }
                                }
                                catch {DispatchQueue.main.async {selectQuizViewController.showAlertView(title: "", message: "Couldn't serialize JSON with error: " + error.localizedDescription)}
                                    
                                }
                            } else { //self.handleNetworkError(data: data, response: response, error: error)
                                
                            }
                        }
                        task.resume()
                    } else {  DispatchQueue.main.async {selectQuizViewController.showAlertView(title: "", message: "Couldn't convert string into URL. Check for special characters.")}
                        
                    }
                }
                catch {  DispatchQueue.main.async {selectQuizViewController.showAlertView(title: "", message: "Couldn't create JSON with error: " + error.localizedDescription)}
                    
                }
            }
            catch {  DispatchQueue.main.async {selectQuizViewController.showAlertView(title: "", message: "Couldn't read receipt data with error: " + error.localizedDescription)}
                
            }
//            DispatchQueue.main.async {
//                LoaderController.sharedInstance.removeLoader()
//            }
        } else {
            
            DispatchQueue.main.async {selectQuizViewController.showAlertView(title: "", message: "No receipt found even though there is an indication something has been purchased before")}
            print("^No receipt found. Need to refresh receipt.")
            UserDefaults.standard.setValue("isRefreshInApp", forKey: "InAppRefresh")
            self.refreshReceipt()
        }
//
    }
    
    private func refreshReceipt() {
        let request = SKReceiptRefreshRequest(receiptProperties: nil)
        request.delegate = self
        request.start()
    }
    
    func convertDateFormat(inputDate: String) -> String {
        
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let oldDate = olDateFormatter.date(from: inputDate)
        
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "MMM dd yyyy h:mm a"
        
        return convertDateFormatter.string(from: oldDate!)
    }
}

extension IAPHandler: SKProductsRequestDelegate, SKPaymentTransactionObserver{// MARK: - REQUEST IAP PRODUCTS
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        //        print("response.products.count ----",response.products.count)
        if (response.products.count > 0) {
            iapProducts = response.products
            for product in iapProducts{
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .currency
                numberFormatter.locale = product.priceLocale
                let price1Str = numberFormatter.string(from: product.price)
                print(product.productIdentifier+product.localizedDescription + "\nfor just \(price1Str!)")
            }
        }
    }
    
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
                print("paymentQueueRestoreCompletedTransactionsFinished")
        selectQuizViewController.setArrays()
        purchaseStatusBlock?(.restored)
    }
    
    // values coming in this sequence from bturner appstore account
    //    Airbrakes_endorsement - - - 0 index
    //    Combination_key_package - - - 1 index
    //    Commercial_vehicle - - - 2 index
    //    Doubles_triples_endorsement - - - 3 index
    //    Full_key_package - - - 4 index
    //    General_knowledge_key_package - - - 5 index
    //    Hazmat_endorsement - - - 6 index
    //    Passenger_bus_endorsement - - - 7 index
    //    Permit_pass_package - - - 8 index
    //    Pretrip_inspection - - - 9 index
    //    School_bus_endorsement - - - 10 index
    //    Tanker_endorsement - - - 11 index
    
    // MARK:- IAP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    print("purchased")
                    UserDefaults.standard.set("true", forKey: "isPurchased")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    //tryCheckValidateReceiptAndUpdateExpirationDate()
                    //                    purchaseStatusBlock?(.purchased)
                    let obj = SelectQuizViewController()
                    obj.allowQuiz()
                    obj.setArrays()
                    
                    print("selectProductIndex",selectProductIndex,UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")
                    if selectProductIndex == 0{
                        obj.updateExpireDate(productTag: "air-brake", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 1{
                        obj.updateExpireDate(productTag: "combination", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 2{
                        obj.updateExpireDate(productTag: "commercial-vehicle", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 3{
                        obj.updateExpireDate(productTag: "doubles-triples", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 4{
                        obj.updateExpireDate(productTag: "full-key", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 5{
                        obj.updateExpireDate(productTag: "general-knowledge", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 6{
                        obj.updateExpireDate(productTag: "hazmat", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 7{
                        obj.updateExpireDate(productTag: "passenger", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 8{
                        obj.updateExpireDate(productTag: "permit-pass", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 9{
                        obj.updateExpireDate(productTag: "pri-trip", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 10{
                        obj.updateExpireDate(productTag: "school-bus", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }else if selectProductIndex == 11{
                        obj.updateExpireDate(productTag: "tanker", productExpiry: "\(UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate") ?? "")")
                    }
                    
                    
                    
                    
                    
                    selectQuizViewController.collectionview.reloadData()
                    if isKeyPresentInUserDefaults(key: "userId"){
                        
                        UIView.transition(with: selectQuizViewController.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                            let _ = SyncView(frame: selectQuizViewController.view.frame, viewController: selectQuizViewController, topView: selectQuizViewController.view)
                            })
                        
                        
                        print("updatedTransactions")
                        
                    }else{
                        let alertController = UIAlertController(title: "Alert", message: "Please do login or registration to sync your details to other devices.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Register", style: .default, handler: { _ in
                            let _ = RegisterView(frame: selectQuizViewController.view.frame, viewController: selectQuizViewController, topView: selectQuizViewController.view)
                        }))
                        alertController.addAction(UIAlertAction(title: "Login", style: .default, handler: { _ in
                            let _ = LoginView(frame: selectQuizViewController.view.frame, viewController: selectQuizViewController, topView: selectQuizViewController.view)
                        }))
                        selectQuizViewController.present(alertController, animated: true, completion: nil)
                    }
                    break
                case .failed:
                    print("failed")
                    let alertView = UIAlertController(title: "", message: "Transaction Failed", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    })
                    alertView.addAction(action)
                    selectQuizViewController.present(alertView, animated: true, completion: nil)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .restored:
                    print("restored")
                    print(trans.payment.productIdentifier)
                    switch trans.payment.productIdentifier{
                    case "Full_key_package":
                        UserDefaults.standard.set(1, forKey: "quiz\(0)")
                        break
                    case "Permit_pass_package":
                        UserDefaults.standard.set(1, forKey: "quiz\(1)")
                        break
                    case "Combination_key_package":
                        UserDefaults.standard.set(1, forKey: "quiz\(2)")
                        break
                    case "General_knowledge_key_package":
                        UserDefaults.standard.set(1, forKey: "quiz\(3)")
                        break
                    case "Pretrip_inspection":
                        UserDefaults.standard.set(1, forKey: "quiz\(4)")
                        break
                    case "Commercial_vehicle":
                        UserDefaults.standard.set(1, forKey: "quiz\(5)")
                        break
                    case "Airbrakes_endorsement":
                        UserDefaults.standard.set(1, forKey: "quiz\(6)")
                        break
                    case "Hazmat_endorsement":
                        UserDefaults.standard.set(1, forKey: "quiz\(7)")
                        break
                    case "Tanker_endorsement":
                        UserDefaults.standard.set(1, forKey: "quiz\(8)")
                        break
                    case "Passenger_bus_endorsement":
                        UserDefaults.standard.set(1, forKey: "quiz\(9)")
                        break
                    case "Doubles_triples_endorsement":
                        UserDefaults.standard.set(1, forKey: "quiz\(10)")
                        break
                    case "School_bus_endorsement":
                        UserDefaults.standard.set(1, forKey: "quiz\(11)")
                        break
                    default:
                        break
                    }
                    UserDefaults.standard.synchronize()
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                default: break
                }}}
    }
}





class QuizCell: UICollectionViewCell{
    @IBOutlet var lbl: UILabel!
    @IBOutlet var imgview: UIImageView!
    @IBOutlet var backview: UIView!
    override func layoutSubviews(){
        backview.frame = CGRect(x: 0, y: 0, width: (UIScreen.main.bounds.width/2) - 30, height: (UIScreen.main.bounds.width/2) - 30)
        let width0 = (UIScreen.main.bounds.width/2) - 30
        let height0 = (UIScreen.main.bounds.width/2) - 30
        imgview.frame = CGRect(x: (width0 - (width0/2))/2, y: height0 * 0.1, width: (width0/2), height: (width0/2))
        lbl.frame = CGRect(x: 5, y: height0 * 0.55, width: width0 - 15, height: 60)
    }
}
var selectQuizViewController = SelectQuizViewController()
class SelectQuizViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var array_image2 = ["full128","permit128","combo128","book128","inspect128","truck128","brakes128","haz128","tanker128","passenger128","double128","school128"]
    
    var array_quiz2 = ["Full Key Package","Permit Pass Package","Combination Vehicle","General Knowledge","Pre- Trip Inspection","Commercial Vehicle","Air Brakes Endorsements","Hazmat Endorsement","Tanker Endorsement","Passenger Endorsement","Double Endorsement","School Bus Endorsement"] //total 12
    
    //    var array_quiz2_package = ["Full Key Package\n$74.99","Permit Pass Package\n$34.99","Combination Vehicle\n$9.99","General Knowledge\n$9.99","Pre- Trip Inspection\n$9.99","Commercial Vehicle\n$9.99","Air Brakes Endorsements\n$9.99","Hazmat Endorsement\n$9.99","Tanker Endorsement\n$9.99","Passenger Endorsement\n$9.99","Double Endorsement\n$9.99","School Bus Endorsement\n$9.99"]
    var array_quiz2_package = ["buyTexasCdlFullDialog".localized(), "buyTexasCdlPermitDialog".localized(), "buyCombinationDialog".localized(), "buyGeneralKnowledgeDialog".localized(), "buyPretripDialog".localized(), "buyTexasCommercialDialog".localized(), "buyAirbrakesDialog".localized(), "buyHazmatDialog".localized(), "buyTankerDialog".localized(), "buyPassengerDialog".localized(), "buyDoublesDialog".localized(), "buySchoolDialog".localized()]
    
    var array_quiz2_text = ["buyTexasCdlFullContent".localized(), "buyTexasCdlPermitContent".localized(), "buyCombinationContent".localized(), "buyGeneralKnowledgeContent".localized(), "buyPretripContent".localized(), "buyTexasCommercialContent".localized(), "buyAirbrakesContent".localized(), "buyHazmatContent".localized(), "buyTankerContent".localized(), "buyPassengerContent".localized(), "buyDoublesContent".localized(),"buySchoolContent".localized()]
    
    
    //    var array_quiz2_text =
    //        ["The real TXDPS Exam questions and answers.\nKey Package to all 10 Exams!\n* - Texas Commercial Vehicle\n* - Combination\n* - General Knowledge\n* - Air Brakes Endorsement\n* - Pre-Trip Inspection\n* - Hazmat Endorsement\n* - Doubles Triples Endorsement\n* - Tanker Endorsement\n* - School Bua Endorsement\n* - Passenger Endorsement",
    //         "The five CDL written Exams you will have to complete to advance on to the road test to obtain your permit.\n* - Texas Commercial Vehicle\n* - Combination\n* - General Knowledge\n* - Air Brakes Endorsement\n* - Pre-Trip Inspection",
    //         "The second Exam you will take in obtaining a permit.\nThere are 50 possible questions. 20/16 (questions asked / must get right)",
    //         "The third Exam you will take to obtaining a permit.\nThere are 160 possible questions. 50/40 (questions asked / must get right)",
    //         "The fifth Exam you will take to obtaining a permit.\nThere are 54 possible questions. 30/24 (questions asked / must get right)",
    //         "The first Exam you will take to obtaining a permit.\nThere are 46 possible questions. 20/16 (questions asked / must get right)",
    //         "The fourth Exam you will take to obtaining a permit.\nThere are 57 possible questions. 25/20 (questions asked / must get right)",
    //         "There are 77 possible question, 30/24 (questions asked / must get right)",
    //         "There are 46 possible question, 20/16 (questions asked / must get right)",
    //         "There are 50 possible question, 20/16 (questions asked / must get right)",
    //         "There are 50 possible question, 20/16 (questions asked / must get right)",
    //         "There are 36 possible question, 20/16 (questions asked / must get right)"
    //    ]
    
    
    var product_key = ["Full_key_package","Permit_pass_package","Combination_key_package","General_knowledge_key_package","Pretrip_inspection","Commercial_vehicle","Airbrakes_endorsement","Hazmat_endorsement","Tanker_endorsement","Passenger_bus_endorsement","Doubles_triples_endorsement","School_bus_endorsement"]
    var indexOfKeyPackages = [4,8,1,5,9,2,0,6,11,7,3,10]
    // values coming in this sequence from bturner appstore account
    //    Airbrakes_endorsement - - - 0 index
    //    Combination_key_package - - - 1 index
    //    Commercial_vehicle - - - 2 index
    //    Doubles_triples_endorsement - - - 3 index
    //    Full_key_package - - - 4 index
    //    General_knowledge_key_package - - - 5 index
    //    Hazmat_endorsement - - - 6 index
    //    Passenger_bus_endorsement - - - 7 index
    //    Permit_pass_package - - - 8 index
    //    Pretrip_inspection - - - 9 index
    //    School_bus_endorsement - - - 10 index
    //    Tanker_endorsement - - - 11 index
    let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    
    let mainview = UIView()
    let titleLabel = UILabel()
    let imageview = UIImageView()
    let lbl_description = UILabel()
    let textView = UITextView()
    let btn_no = UIButton()
    let btn_continue = UIButton()
    let blackview = UIView()
    static var selected_view = Int()
    var flag_arrayIsSet = Int()
    static var chosen_package = String()
    @IBOutlet var collectionview: UICollectionView!
    var selected_index = 0
    var curentdate = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        activityView.hidesWhenStopped = true
        activityView.isHidden = true
        setUpMenuButton()
        let btn_restore = UIBarButtonItem(title:"Restore Purchase", style: .plain, target: self, action: #selector(selectQuizViewController.restore))
        btn_restore.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        self.navigationItem.rightBarButtonItem = btn_restore
        self.navigationController?.navigationBar.tintColor = .white
        //        UserDefaults.standard.set(1, forKey: "quiz1")// : Uncomment this if you want to access all quizes.
        
        
        selectQuizViewController = self
        curentdate =  Date.changeDaysBy(days: 0)
        //        collectionview.delegate = self
        //        collectionview.dataSource = self
        if isKeyPresentInUserDefaults(key: "userId"){
            getAllOrder(showAlert: true)
        }
        //
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isDataGetEmpty = true
        if UserDefaults.standard.value(forKey: "InAppRefresh") as? String == "isRefreshInApp"{
            IAPHandler.shared.tryCheckValidateReceiptAndUpdateExpirationDate()
        }
        setArrays()
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("methodOfReceivedNotification")
        DispatchQueue.main.async {
            self.getAllOrder()
        }
        
    }
    
    @objc func restore(){
        IAPHandler.shared.restorePurchase()
        print("purchase restored")
        setArrays()
        UserDefaults.standard.set(true, forKey: "Restore")
    }
    
    func updateExpireDate(productTag:String,productExpiry:String) {
        selectProductIndex = -1
        UserDefaults.standard.removeObject(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate")
        fetchGenericData(urlString: "order_place", parameters: "user_id=\(UserDefaults.standard.value(forKey: "userId")!)&product_tag=\(productTag)&expire_date=\(productExpiry)", method: "POST", completion: {(objCommonResponse: AllOrdersLogin?, str) in
            DispatchQueue.main.async {
                if let objCommonResponse = objCommonResponse{
                    if objCommonResponse.status {
                        
                        //self.showAlertView(title: "Alert", message: "Synced successfully")
                        //selectQuizViewController.getAllOrder()
                    }else{
                        self.showAlertView(title: "Alert", message: objCommonResponse.message)
                    }
                }
                LoaderController.sharedInstance.removeLoader()
            }
        })
    }
    
    
    
    func setArrays(){
        for i in 0..<12{
            if isKeyPresentInUserDefaults(key: "quiz\(i)"){
                print("quiz\(i) already purchased")
                if i == 1{
                    print("only permit pass bought")
                    UserDefaults.standard.set(["Combination Vehicle","General Knowledge","Pre- Trip Inspection","Commercial Vehicle","Air Brakes Endorsements","Hazmat Endorsement","Tanker Endorsement","Passenger Endorsement","Double Endorsement","School Bus Endorsement"], forKey: "array_12_text")
                    UserDefaults.standard.set(["combochecked128","bookschecked128","inspectchecked128","truckchecked128","brakeschecked128","haz128","tanker128","passenger128","double128","school128"], forKey: "array_12_image")
                    
                    UserDefaults.standard.set(1, forKey: "quiz\(2)")
                    UserDefaults.standard.set(1, forKey: "quiz\(3)")
                    UserDefaults.standard.set(1, forKey: "quiz\(4)")
                    UserDefaults.standard.set(1, forKey: "quiz\(5)")
                    UserDefaults.standard.set(1, forKey: "quiz\(6)")
                    flag_arrayIsSet = 1
                    
                    if isKeyPresentInUserDefaults(key: "quiz\(7)"){
                        var obj_array_image = UserDefaults.standard.value(forKey: "array_12_image") as! [String]
                        obj_array_image[5] = "hazchecked128"
                        UserDefaults.standard.set(obj_array_image, forKey: "array_12_image")
                    }
                    if isKeyPresentInUserDefaults(key: "quiz\(8)"){
                        var obj_array_image = UserDefaults.standard.value(forKey: "array_12_image") as! [String]
                        obj_array_image[6] = "tankerchecked128"
                        UserDefaults.standard.set(obj_array_image, forKey: "array_12_image")
                    }
                    if isKeyPresentInUserDefaults(key: "quiz\(9)"){
                        var obj_array_image = UserDefaults.standard.value(forKey: "array_12_image") as! [String]
                        obj_array_image[7] = "passengerchecked128"
                        UserDefaults.standard.set(obj_array_image, forKey: "array_12_image")
                    }
                    if isKeyPresentInUserDefaults(key: "quiz\(10)"){
                        var obj_array_image = UserDefaults.standard.value(forKey: "array_12_image") as! [String]
                        obj_array_image[8] = "doublechecked128"
                        UserDefaults.standard.set(obj_array_image, forKey: "array_12_image")
                    }
                    if isKeyPresentInUserDefaults(key: "quiz\(11)"){
                        var obj_array_image = UserDefaults.standard.value(forKey: "array_12_image") as! [String]
                        obj_array_image[9] = "schoolchecked128"
                        UserDefaults.standard.set(obj_array_image, forKey: "array_12_image")
                    }
                    return
                } else if i == 0{
                    print("full package bought")
                    UserDefaults.standard.set(["Combination Vehicle","General Knowledge","Pre- Trip Inspection","Commercial Vehicle","Air Brakes Endorsements","Hazmat Endorsement","Tanker Endorsement","Passenger Endorsement","Double Endorsement","School Bus Endorsement"], forKey: "array_12_text")
                    UserDefaults.standard.set(["combochecked128","bookschecked128","inspectchecked128","truckchecked128","brakeschecked128","hazchecked128","tankerchecked128","passengerchecked128","doublechecked128","schoolchecked128"], forKey: "array_12_image")
                    UserDefaults.standard.set(1, forKey: "quiz\(2)")
                    UserDefaults.standard.set(1, forKey: "quiz\(3)")
                    UserDefaults.standard.set(1, forKey: "quiz\(4)")
                    UserDefaults.standard.set(1, forKey: "quiz\(5)")
                    UserDefaults.standard.set(1, forKey: "quiz\(6)")
                    UserDefaults.standard.set(1, forKey: "quiz\(7)")
                    UserDefaults.standard.set(1, forKey: "quiz\(8)")
                    UserDefaults.standard.set(1, forKey: "quiz\(9)")
                    UserDefaults.standard.set(1, forKey: "quiz\(10)")
                    UserDefaults.standard.set(1, forKey: "quiz\(11)")
                    flag_arrayIsSet = 1
                    return
                }
                UserDefaults.standard.synchronize()
            }
        }
        if flag_arrayIsSet == 0{
            print("all values visible")
            UserDefaults.standard.set(["Full Key Package","Permit Pass Package","Combination Vehicle","General Knowledge","Pre- Trip Inspection","Commercial Vehicle","Air Brakes Endorsements","Hazmat Endorsement","Tanker Endorsement","Passenger Endorsement","Double Endorsement","School Bus Endorsement"], forKey: "array_12_text")
            var arr = ["full128","permit128","combo128","book128","inspect128","truck128","brakes128","haz128","tanker128","passenger128","double128","school128"]
            if isKeyPresentInUserDefaults(key: "quiz2"){
                arr[2] = "combochecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz3"){
                arr[3] = "bookschecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz4"){
                arr[4] = "inspectchecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz5"){
                arr[5] = "truckchecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz6"){
                arr[6] = "brakeschecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz7"){
                arr[7] = "hazchecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz8"){
                arr[8] = "tankerchecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz9"){
                arr[9] = "passengerchecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz10"){
                arr[10] = "doublechecked128"
            }
            if isKeyPresentInUserDefaults(key: "quiz11"){
                arr[11] = "schoolchecked128"
            }
            UserDefaults.standard.set(arr, forKey: "array_12_image")
        }
        UserDefaults.standard.synchronize()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionview.reloadData()
    }
    
    func allowQuiz(){
        UserDefaults.standard.set(1, forKey: "quiz\(SelectQuizViewController.selected_view)")
        guard let currentDate = Calendar.current.date(byAdding: .month, value: 1, to: Date()) else {return;}
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let expireDate = formatter.string(from: currentDate)
        print("setting expireDate: "+expireDate+" for quiz:\(SelectQuizViewController.selected_view)")
        UserDefaults.standard.set(expireDate, forKey: "quiz\(SelectQuizViewController.selected_view)_expDate")
        print("setting:-",UserDefaults.standard.value(forKey: "quiz\(SelectQuizViewController.selected_view)_expDate"))
        if SelectQuizViewController.selected_view == 1{
            //permit pass
            UserDefaults.standard.set(expireDate, forKey: "quiz\(2)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(3)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(4)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(5)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(6)")
        }else if SelectQuizViewController.selected_view == 0{
            //full key
            UserDefaults.standard.set(expireDate, forKey: "quiz\(2)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(3)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(4)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(5)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(6)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(7)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(8)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(9)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(10)")
            UserDefaults.standard.set(expireDate, forKey: "quiz\(11)")
        }
        UserDefaults.standard.synchronize()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let obj_array = UserDefaults.standard.value(forKey: "array_12_text") as! [String]
        return obj_array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizCell", for: indexPath) as! QuizCell
        let obj_array_text = UserDefaults.standard.value(forKey: "array_12_text") as! [String]
        switch obj_array_text[indexPath.row]{
        case "Full Key Package":
            cell.lbl.text = "app_selectionfull".localized().capitalized
        case "Permit Pass Package":
            cell.lbl.text = "app_selectionpermit".localized().capitalized
        case "Combination Vehicle":
            cell.lbl.text = "app_combo".localized().capitalized
        case "General Knowledge":
            cell.lbl.text = "app_general".localized().capitalized
        case "Pre- Trip Inspection":
            cell.lbl.text = "app_pretrip".localized().capitalized
        case "Commercial Vehicle":
            cell.lbl.text = "app_txcomm".localized().capitalized
        case "Air Brakes Endorsements":
            cell.lbl.text = "app_airbrakes".localized().capitalized
        case "Hazmat Endorsement":
            cell.lbl.text = "app_hazmat".localized().capitalized
        case "Tanker Endorsement":
            cell.lbl.text = "app_tanker".localized().capitalized
        case "Passenger Endorsement":
            cell.lbl.text = "app_passenger".localized().capitalized
        case "Double Endorsement":
            cell.lbl.text = "app_double".localized().capitalized
        case "School Bus Endorsement":
            cell.lbl.text = "app_school".localized().capitalized
        default:
            break;
        }
        let obj_array_image = UserDefaults.standard.value(forKey: "array_12_image") as! [String]
        cell.imgview.image = UIImage(named: obj_array_image[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width/2) - 35, height: (UIScreen.main.bounds.width/2) - 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let exp_date_str = UserDefaults.standard.value(forKey: "quiz\(indexPath.row)_expDate") as? String
        print("expDate~~0~\(exp_date_str)~~~",isKeyPresentInUserDefaults(key: "quiz\(indexPath.row)_expDate"),isKeyPresentInUserDefaults(key: "quiz\(indexPath.row)"))
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if isKeyPresentInUserDefaults(key: "quiz0"){
            let next = storyboard?.instantiateViewController(withIdentifier: "StartQuizViewController") as! StartQuizViewController
            next.quiz_number = indexPath.row
            self.navigationController?.pushViewController(next, animated: true)
        }else if isKeyPresentInUserDefaults(key: "quiz1"){
            if isKeyPresentInUserDefaults(key: "quiz\(indexPath.row+2)"){
                if isKeyPresentInUserDefaults(key: "quiz\(indexPath.row+2)_expDate"){
                    if let exp_date_str = UserDefaults.standard.value(forKey: "quiz\(indexPath.row+2)_expDate") as? String{
                        if let exp_date = formatter.date(from: exp_date_str){
                            if Date() > exp_date{
                                showAlertView(title: "Alert", message: "Plan has expired. Please repurchase.")
                                UserDefaults.standard.removeObject(forKey: "quiz\(indexPath.row+2)")
                                UserDefaults.standard.removeObject(forKey: "quiz\(indexPath.row+2)_expDate")
                                UserDefaults.standard.synchronize()
                                return;
                            }
                        }
                    }
                }
                let next = storyboard?.instantiateViewController(withIdentifier: "StartQuizViewController") as! StartQuizViewController
                next.quiz_number = indexPath.row
                self.navigationController?.pushViewController(next, animated: true)
            }else{
                
                setUpView(selectedIndex: indexPath.row + 2)
                SelectQuizViewController.selected_view = indexPath.row + 2
                selected_index = indexOfKeyPackages[indexPath.row + 2]
                selectProductIndex = indexOfKeyPackages[indexPath.row + 2]
                print("selected_index",selected_index)
                SelectQuizViewController.chosen_package = product_key[indexPath.row + 2]
            }
        }else{
            if isKeyPresentInUserDefaults(key: "quiz\(indexPath.row)"){
                print("expDate~~1~\(exp_date_str)~~~",isKeyPresentInUserDefaults(key: "quiz\(indexPath.row)_expDate"))
                if isKeyPresentInUserDefaults(key: "quiz\(indexPath.row)_expDate"){
                    if let exp_date_str = UserDefaults.standard.value(forKey: "quiz\(indexPath.row)_expDate") as? String{
                        if let exp_date = formatter.date(from: exp_date_str){
                            if Date() > exp_date{
                                showAlertView(title: "Alert", message: "Plan has expired. Please repurchase.")
                                UserDefaults.standard.removeObject(forKey: "quiz\(indexPath.row)")
                                UserDefaults.standard.removeObject(forKey: "quiz\(indexPath.row)_expDate")
                                UserDefaults.standard.synchronize()
                                return;
                            }
                        }
                    }
                }else if isKeyPresentInUserDefaults(key: "quiz\(indexPath.row)_expDate") == false {
                    showAlertView(title: "Alert", message: "Plan has expired. Please repurchase.")
                    UserDefaults.standard.removeObject(forKey: "quiz\(indexPath.row)")
                    UserDefaults.standard.removeObject(forKey: "quiz\(indexPath.row)_expDate")
                    UserDefaults.standard.synchronize()
                    return;
                }
                let next = storyboard?.instantiateViewController(withIdentifier: "StartQuizViewController") as! StartQuizViewController
                next.quiz_number = indexPath.row - 2
                self.navigationController?.pushViewController(next, animated: true)
            }else{
                setUpView(selectedIndex: indexPath.row)
                SelectQuizViewController.selected_view = indexPath.row
                SelectQuizViewController.chosen_package = product_key[indexPath.row]
                selected_index = indexOfKeyPackages[indexPath.row]
                selectProductIndex = indexOfKeyPackages[indexPath.row]
                print("selected_index~~~~1~",selected_index,indexPath.row,product_key[indexPath.row])
            }
        }
    }
    
    fileprivate func setUpView(selectedIndex: Int){
        blackview.backgroundColor = UIColor(white: 0, alpha: 0.5)
        blackview.frame = view.frame
        blackview.alpha = 1
        
        imageview.image = UIImage(named: array_image2[selectedIndex])
        textView.text = array_quiz2_text[selectedIndex]
        
        titleLabel.text = array_quiz2_package[selectedIndex]
        titleLabel.numberOfLines = 0
        btn_no.setTitle("NO THANKS", for: .normal)
        btn_continue.setTitle("CONTINUE", for: .normal)
        btn_continue.contentHorizontalAlignment = .left
        btn_continue.addTarget(self, action: #selector(purchase_product_popup), for: .touchUpInside)
        btn_no.contentHorizontalAlignment = .left
        mainview.backgroundColor = UIColor.white
        
        mainview.addSubview(imageview)
        mainview.addSubview(titleLabel)
        mainview.addSubview(textView)
        mainview.addSubview(btn_continue)
        mainview.addSubview(btn_no)
        blackview.addSubview(mainview)
        self.view.addSubview(blackview)
        
        imageview.topAnchor.constraint(equalTo: mainview.topAnchor, constant: 30).isActive = true
        imageview.leadingAnchor.constraint(equalTo: mainview.leadingAnchor, constant: 30).isActive = true
        imageview.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageview.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageview.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.topAnchor.constraint(equalTo: mainview.topAnchor, constant: 30).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: imageview.trailingAnchor, constant: 20).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        //        textView.topAnchor.constraint(equalTo: imageview.bottomAnchor, constant: 30).isActive = true
        //        textView.leadingAnchor.constraint(equalTo: mainview.leadingAnchor, constant: 30).isActive = true
        //        textView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.7).isActive = true
        //textView.numberOfLines = 0
        textView.isEditable = false
        textView.dataDetectorTypes = .link
        textView.font = UIFont.systemFont(ofSize: 18)
        textView.anchor(top: imageview.bottomAnchor, paddingTop: 30, bottom: nil, paddingBottom: 0, left: mainview.leftAnchor, paddingLeft: 5, right: mainview.rightAnchor, paddingRight: 5, width: 0, height: 150)
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        btn_continue.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 30).isActive = true
        btn_continue.trailingAnchor.constraint(equalTo: mainview.trailingAnchor, constant: 30).isActive = true
        btn_continue.widthAnchor.constraint(equalToConstant: 150).isActive = true
        btn_continue.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btn_continue.translatesAutoresizingMaskIntoConstraints = false
        btn_continue.setTitleColor(UIColor.blue, for: .normal)
        
        btn_no.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 30).isActive = true
        btn_no.trailingAnchor.constraint(equalTo: btn_continue.leadingAnchor, constant: 30).isActive = true
        btn_no.widthAnchor.constraint(equalToConstant: 150).isActive = true
        btn_no.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btn_no.translatesAutoresizingMaskIntoConstraints = false
        btn_no.setTitleColor(UIColor.blue, for: .normal)
        btn_no.addTarget(self, action: #selector(removeView), for: .touchUpInside)
        
        NSLayoutConstraint(item: mainview, attribute: .centerY, relatedBy: .equal, toItem: blackview, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        mainview.leadingAnchor.constraint(equalTo: blackview.leadingAnchor, constant: 20).isActive = true
        mainview.trailingAnchor.constraint(equalTo: blackview.trailingAnchor, constant: -20).isActive = true
        mainview.bottomAnchor.constraint(equalTo: btn_no.bottomAnchor, constant: 20).isActive = true
        mainview.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @objc func purchase_product_popup(){
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                print("purchased")
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
        }
        removeView()
        let alertController = UIAlertController(title: "Make Purchase", message: "Are you sure you want to purchase this plan?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: self.purchase)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(OKAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    func purchase(_:UIAlertAction){
        print("purchasing ",selected_index)
        IAPHandler.shared.purchaseMyProduct(index: selected_index)
    }
    
    @objc func removeView(){
        imageview.removeFromSuperview()
        titleLabel.removeFromSuperview()
        lbl_description.removeFromSuperview()
        btn_continue.removeFromSuperview()
        btn_no.removeFromSuperview()
        mainview.removeFromSuperview()
        blackview.removeFromSuperview()
    }
    
    func changeLanguage(){
        print("selectquizVC")
        array_quiz2_package = ["buyTexasCdlFullDialog".localized(), "buyTexasCdlPermitDialog".localized(), "buyCombinationDialog".localized(), "buyGeneralKnowledgeDialog".localized(), "buyPretripDialog".localized(), "buyTexasCommercialDialog".localized(), "buyAirbrakesDialog".localized(), "buyHazmatDialog".localized(), "buyTankerDialog".localized(), "buyPassengerDialog".localized(), "buyDoublesDialog".localized(), "buySchoolDialog".localized()]
        collectionview.reloadData()
    }
}

extension Date {
    static func changeDaysBy(days : Int) -> Date {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.day = days
        return Calendar.current.date(byAdding: dateComponents, to: currentDate)!
    }
}
