//
//  ForgotPassView.swift
//  TEXAS CDL PREP
//
//  Created by Anushka on 07/04/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit




class ForgotPassView: UIView {

    let imgView = UIImageView()
    let lblHeading = UILabel()
    let lblStr = UILabel()
    let txtEmail = CommonTextfield()
    
    let btnOk = UIButton(type: .system)
    let btnCancel = UIButton(type: .system)
//    let btnCross = UIButton(type: .system)
    let blackview = UIView()
    var topview: UIView!
    var vc: UIViewController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, viewController: UIViewController, topView: UIView) {
        self.init(frame: frame)
        topview = topView
        vc = viewController
        
        txtEmail.placeholder = "Enter Email"
        if #available(iOS 13.0, *) {
            txtEmail.setImage(image: UIImage(systemName: "envelope.fill")!, leftPadding: 20)
        } else {
            // Fallback on earlier versions
        }
        lblHeading.text = "Forgot Password?"
        lblHeading.textAlignment = .center
        lblHeading.font = UIFont.boldSystemFont(ofSize: 18)
        lblHeading.textColor = appcolor
        lblStr.text = "To reset password, enter your email and check mail to follow instruction."
        lblStr.textAlignment = .center
        lblStr.numberOfLines = 0
        lblStr.lineBreakMode = .byWordWrapping
        //btnCancel.backgroundColor = appcolor
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.tintColor = appcolor
        btnCancel.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
//        btnCancel.layer.cornerRadius = 20
        
        //btnCancel.backgroundColor = appcolor
        btnOk.setTitle("OK", for: .normal)
        btnOk.tintColor = appcolor
        btnOk.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
//        btnCancel.layer.cornerRadius = 20
        
        
        
//        btnCross.setImage(UIImage(named: "close.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        blackview.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
        setUpView(viewController: viewController, topView: topView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpView(viewController: UIViewController, topView: UIView){
        
        let width = CGFloat(screenWidth * 0.9)
        let textHeight = CGFloat(35)
        let topSpace = CGFloat(15)
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 5
        
        topView.addSubview(blackview)
        blackview.anchor(top: topView.topAnchor, paddingTop: 0, bottom: topView.bottomAnchor, paddingBottom: 0, left: topView.leftAnchor, paddingLeft: 0, right: topView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        
        blackview.addSubview(self)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.centerXAnchor.constraint(equalTo: blackview.centerXAnchor, constant: 0).isActive = true
        self.centerYAnchor.constraint(equalTo: blackview.centerYAnchor, constant: 0).isActive = true
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.backgroundColor = .white
        
//        self.addSubview(btnCross)
//        btnCross.anchor(top: self.topAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: 50, height: 50)
//        btnCross.addTarget(self, action: #selector(removeDynamicView), for: .touchUpInside)
        
        self.addSubview(lblHeading)
        lblHeading.anchor(top: self.topAnchor, paddingTop: topSpace, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: screenWidth * 0.5, height: 20)
        lblHeading.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(lblStr)
        lblStr.anchor(top: lblHeading.bottomAnchor, paddingTop: topSpace, bottom: nil, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 10, right: self.rightAnchor, paddingRight: 10, width: screenWidth-20, height: 50)
        lblStr.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.addSubview(txtEmail)
        txtEmail.translatesAutoresizingMaskIntoConstraints = false
        txtEmail.topAnchor.constraint(equalTo: lblStr.bottomAnchor, constant: topSpace).isActive = true
        txtEmail.heightAnchor.constraint(equalToConstant: textHeight).isActive = true
        txtEmail.widthAnchor.constraint(equalToConstant: width * 0.8).isActive = true
        txtEmail.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        txtEmail.keyboardType = .emailAddress
        
        
        
        self.addSubview(btnCancel)
        btnCancel.anchor(top: txtEmail.bottomAnchor, paddingTop: 15, bottom: self.bottomAnchor, paddingBottom: 10, left: self.leftAnchor, paddingLeft: 30, right: nil, paddingRight: 0, width: 100, height: 30)
        btnCancel.addTarget(self, action: #selector(onClickCancel), for: .touchUpInside)
        
        self.addSubview(btnOk)
        btnOk.anchor(top: txtEmail.bottomAnchor, paddingTop: 15, bottom: self.bottomAnchor, paddingBottom: 10, left: nil, paddingLeft: 0, right: self.rightAnchor, paddingRight: 30, width: 100, height: 30)
        btnOk.addTarget(self, action: #selector(callForgotApi), for: .touchUpInside)
        
        
        
    }
    
    @objc func onClickCancel(){
        self.removeDynamicView()
    }
    
    @objc func removeDynamicView(){
        self.removeFromSuperview()
        self.blackview.removeFromSuperview()
    }
    
    @objc func callForgotApi(){
        if txtEmail.text == ""{
            vc.showAlertView(title: "Alert", message: "Cannot leave any field empty.")
            return;
        }
        if ((txtEmail.text?.isValidEmail) == false){
            vc.showAlertView(title: "Alert", message: "Please enter correct email.")
            return;
        }
        fetchGenericData(urlString: "forget_pass", parameters: "user_email=\(txtEmail.text!)", completion: {(objCommonResponse: Login?, str) in
            DispatchQueue.main.async {
                
                if ((str?.contains("User not found")) != nil) {
                    self.vc.showAlertView(title: "Alert", message: "User not found")
                    self.removeDynamicView()
                    return
                }
                
                if let objCommonResponse = objCommonResponse{
                    print("objCommonResponse",objCommonResponse)
                    if objCommonResponse.status {
//                        let _ = SyncView(frame: self.vc.view.frame, viewController: self.vc, topView: self.vc.view)
//                        UserDefaults.standard.setValue(objCommonResponse.result![0].ID, forKey: "userId")
//                        UserDefaults.standard.synchronize()
//                        self.vc?.getAllOrder(showAlert: true)
                        self.vc.showAlertView(title: "Alert", message: objCommonResponse.message)
                        self.removeDynamicView()
                    }else{
                        self.vc.showAlertView(title: "Alert", message: objCommonResponse.message)
                    }
                }
                
                LoaderController.sharedInstance.removeLoader()
            }
        })
    }
    
    
    
    deinit {
        print("forgot password view deinit")
    }
    
    
    
}


extension String {
    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
}
