//
//  ContactUsViewController.swift
//  TEXAS CDL PREP
//
//  Created by Apple on 05/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var txt_name: UITextField!
    @IBOutlet var txt_subject: UITextField!
    @IBOutlet var txtview_msg: UITextView!
    @IBOutlet weak var lblContactUs: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "title_activity_contact".localized()
        lblContactUs.text = self.title
        lblContactUs.sizeToFit()
        txt_name.placeholder = "name".localized()
        txt_subject.placeholder = "subject".localized()
        setUpMenuButton()
        setUpRightMenuButton()
        txtview_msg.dropShadow()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendMail(_ sender: Any) {
        let emailTitle = txt_subject.text!
        let messageBody = txtview_msg.text!
        let toRecipents = ["sales@texascdlprep.com"]
        if MFMailComposeViewController.canSendMail() {
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            self.present(mc, animated: true, completion: nil)
        } else {
            showAlertView(title: "Alert", message: "Please Set Up Email First And Then Try Again.")
        }
    }
    
    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(String(describing: error?.localizedDescription))")
        }
        self.dismiss(animated: true, completion: nil)
    }


}
