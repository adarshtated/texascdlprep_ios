//
//  SettingCell.swift
//  PantherApp1
//
//  Created by Adarsh on 06/07/18.
//  Copyright © 2018 Adarsh. All rights reserved.
//

import UIKit
class SettingCell: UICollectionViewCell{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    var setting: Setting?{
        didSet{
            sideLabel.text = setting?.name
            let imagename = setting?.imageName
            iconImageView.image = UIImage(named: imagename!)?.withRenderingMode(.alwaysTemplate)
            iconImageView.tintColor = UIColor.black//init(red: 241/255, green: 92/255, blue: 48/255, alpha: 1.0)//UIColor.darkGray
        }
    }
    
    let sideLabel: UILabel = {
        let label = UILabel()
        label.sizeToFit()
        label.numberOfLines = 0
        return label
    }()
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    func setUpView(){
        addSubview(sideLabel)
        addSubview(iconImageView)
        addConstraintsWithFormat("H:|-20-[v0(30)]-20-[v1]|", views: iconImageView, sideLabel)
        addConstraintsWithFormat("V:|[v0]|", views: sideLabel)
        addConstraintsWithFormat("V:[v0(25)]", views: iconImageView)
        addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

