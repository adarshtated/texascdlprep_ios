//
//  sideMenu.swift
//  PantherApp1
//
//  Created by Adarsh on 05/07/18.
//  Copyright © 2018 Adarsh. All rights reserved.
//
import UIKit
import MessageUI
import Social
import Cosmos

class Setting: NSObject{
    let name: String
    let imageName: String
    init(name: String, imageName: String){
        self.name = name
        self.imageName = imageName
    }
}

class SideMenu:NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    static let sharedInstance = SideMenu()
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    let cellId = "cell"
    let cellId2 = "cell2"
    var show8 = false
    var viewcontroll: UIViewController!
    var settings = [Setting]()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settings.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId2, for: indexPath) as! LogoCell
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SettingCell
            let setting = settings[indexPath.row - 1]
            cell.setting = setting
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: collectionView.frame.width, height: UIScreen.main.bounds.width*0.6)
        }
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var indexpath = 0
        indexpath = indexPath.row - 1
        handleDismiss()
        switch indexpath{
        case 0:
            home()
        case 1:
            contactUs()
        case 2:
            share()
        case 3:
            rateUs()
        default:
            print("wrong case")
        }
        
//        buttonarr[3].addTarget(self, action: #selector(rateUs), for: .touchUpInside)
    }
    @objc func home(){
        viewcontroll.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func contactUs(){
        let next = viewcontroll.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        viewcontroll.navigationController?.pushViewController(next, animated: false)
    }
    
    
    var mainView = UIView()
    var imgview = UIImageView()
    var lbl = UILabel()
    var view_rateUs = CosmosView()
    var btn = UIButton()
    var blackview3 = UIView()
    
    @objc func rateUs(){
        topbackimage.image = UIImage(named: "greygrad1.jpg")
        logoimg.image = UIImage(named: "mbanner223")
        handleDismissWithBlackView()
        if let window = UIApplication.shared.keyWindow{
            let width = window.frame.width * 0.9
            let height = window.frame.height * 0.65
            btn.setTitle("Not Now", for: .normal)
            btn.setTitleColor(UIColor.red, for: .normal)
            lbl.text = "How was your experience with us?"
            lbl.textAlignment = .center
            lbl.font = lbl.font.withSize(25)
            lbl.numberOfLines = 2
            //        mainView.backgroundColor = UIColor.white
            mainView.layer.cornerRadius = 10
            mainView.backgroundColor = UIColor.white
            imgview.image = UIImage(named: "banner")
            view_rateUs.settings.updateOnTouch = true
            // Show only fully filled stars
            view_rateUs.settings.fillMode = .full
            // Other fill modes: .half, .precise
            // Change the size of the stars
            view_rateUs.settings.starSize = 50
            // Set the distance between stars
            view_rateUs.settings.starMargin = 5
            // Set the color of a filled star
            view_rateUs.settings.filledColor = UIColor.blue
            // Set the border color of an empty star
            view_rateUs.settings.emptyBorderColor = UIColor.blue
            // Set the border color of a filled star
            view_rateUs.settings.filledBorderColor = UIColor.blue
            
            mainView.frame = CGRect(x: (window.frame.width - window.frame.width * 0.9)/2, y: (window.frame.height - window.frame.height * 0.65)/2, width: width, height: height)
            imgview.frame = CGRect(x: (width - width*0.4)/2, y: 20, width: width * 0.4, height: width * 0.4)
            lbl = UILabel(frame: CGRect(x: (width - width*0.75)/2, y: height * 0.4, width: width * 0.75, height: 100))
            view_rateUs.center.y = mainView.frame.height * 0.65
            view_rateUs.frame.origin.x = (mainView.frame.width - 270) / 2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openAppstore(tapGestureRecognizer:)))
            view_rateUs.addGestureRecognizer(tapGestureRecognizer)
            btn.frame = CGRect(x: (width - 200)/2, y: height*0.85, width: 200, height: 70)
            btn.addTarget(self, action: #selector(removeRateUs), for: .touchUpInside)
            
            mainView.layer.borderColor = UIColor.darkGray.cgColor
            mainView.layer.borderWidth = 2.0
            blackview3.backgroundColor = UIColor.init(white: 0.5, alpha: 0.5)
            
            mainView.addSubview(imgview)
            mainView.addSubview(lbl)
            mainView.addSubview(view_rateUs)
            mainView.addSubview(btn)
            blackview3.frame = window.frame
            window.addSubview(blackview3)
            window.addSubview(mainView)
        }
    }
    
    let view = UIView()
    let blackview = UIView()
    var topbackimage = UIImageView()
    var logoimg = UIImageView()
    var buttonarr = [UIButton]()
    var imgviewarr = [UIImageView]()
    
    @objc func handleDismissWithBlackView(){
        blackview.removeFromSuperview()
        view.removeFromSuperview()
        topbackimage.removeFromSuperview()
        logoimg.removeFromSuperview()
        for i in 0..<buttonarr.count{
            buttonarr[i].removeFromSuperview()
            imgviewarr[i].removeFromSuperview()
        }
    }
    
    
    @objc func openAppstore(tapGestureRecognizer: UITapGestureRecognizer){
        let url = URL(string: "itms-apps:itunes.apple.com/us/app/apple-store/id1439406536?mt=8&action=write-review")!
        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    
    @objc func share(){
        
        if let urlStr = NSURL(string: "https://itunes.apple.com/us/app/myapp/id1439406536?ls=1&mt=8") {
            let objectsToShare = [urlStr]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                if let popup = activityVC.popoverPresentationController {
                    popup.sourceView = self.view
                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                }
            }
            
            viewcontroll.present(activityVC, animated: true, completion: nil)
        }
    }
    
    
    @objc func removeRateUs(){
        blackview3.removeFromSuperview()
        imgview.removeFromSuperview()
        lbl.removeFromSuperview()
        btn.removeFromSuperview()
        mainView.removeFromSuperview()
    }
    
    
    
    let blackView = UIView()
    let collectionViewSide: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: UIScreen.main.bounds.height), collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    @objc func showSideBar(_ objVC: UIViewController!){
        print("show side bar")
        settings = [Setting(name: "menu_home".localized(), imageName: "home5"), Setting(name: "menu_contact".localized(), imageName: "cellphone69"), Setting(name: "menu_share".localized(), imageName: "share69"), Setting(name: "menu_rate".localized(), imageName: "rating69")]
        self.viewcontroll = objVC
        if let window = UIApplication.shared.keyWindow{
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
            swipeRight.direction = UISwipeGestureRecognizer.Direction.right
            collectionViewSide.addGestureRecognizer(swipeRight)
            
            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
            swipeDown.direction = UISwipeGestureRecognizer.Direction.left
            collectionViewSide.addGestureRecognizer(swipeDown)
            
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            window.addSubview(collectionViewSide)
            blackView.frame = window.frame
            blackView.alpha = 0
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn, animations: {
                self.blackView.alpha = 1
                self.collectionViewSide.frame = CGRect(x: 0, y: 0, width: window.frame.width * 0.8, height: window.frame.height)}, completion: nil)
        }
        self.collectionViewSide.reloadData()
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.left:
                handleDismiss()
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            default:
                break
            }
        }
    }
    
    @objc func handleDismiss(){
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {self.blackView.alpha = 0
            self.collectionViewSide.frame = CGRect(x: 0, y: 0, width: 0, height: self.collectionViewSide.frame.height)}, completion: nil)
    }
    
    
    @objc func handleDismiss2(){
        view.removeFromSuperview()
        topbackimage.removeFromSuperview()
        logoimg.removeFromSuperview()
        for i in 0..<buttonarr.count{
            buttonarr[i].removeFromSuperview()
            imgviewarr[i].removeFromSuperview()
        }
    }
    
    override init(){
        super.init()
//        print("init menu")
        collectionViewSide.dataSource = self
        collectionViewSide.delegate = self
        collectionViewSide.register(SettingCell.self, forCellWithReuseIdentifier: cellId)
        collectionViewSide.register(LogoCell.self, forCellWithReuseIdentifier: cellId2)
    }
    deinit{
//        print("deinit menu")
    }
}
