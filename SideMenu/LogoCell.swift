//
//  LogoCell.swift
//  PantherApp1
//
//  Created by Adarsh on 06/07/18.
//  Copyright © 2018 Adarsh. All rights reserved.
//

import UIKit
class LogoCell: UICollectionViewCell{
        override init(frame: CGRect) {
            super.init(frame: frame)
            setUpView()
        }
        let logo: UIImageView = {
            let menulogo = UIImageView()
            menulogo.image = UIImage(named: "1280x720.png")
            menulogo.frame = CGRect(x: (UIScreen.main.bounds.width*0.8 - UIScreen.main.bounds.width*0.6)/2, y: 30, width: UIScreen.main.bounds.width*0.6, height: UIScreen.main.bounds.width*0.5)
            menulogo.contentMode = .scaleToFill
            return menulogo
        }()
        let backImageView: UIImageView = {
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "greygrad1.jpg")
            backImageView.frame = CGRect(x: (UIScreen.main.bounds.width*0.8 - UIScreen.main.bounds.width*0.6)/2, y: 30, width: UIScreen.main.bounds.width*0.6, height: UIScreen.main.bounds.width*0.5)
            return backImageView
        }()
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        func setUpView(){
            addSubview(backImageView)
            addSubview(logo)
//            addConstraint(NSLayoutConstraint(item: backImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        }
    }
